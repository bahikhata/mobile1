module.exports = {
  resolver: {
    extraNodeModules: {
      // Polyfills for node libraries
      //"crypto": require.resolve("crypto-browserify"),
      //"stream": require.resolve("stream-browserify")
    }
  },
  transformer: {
    getTransformOptions: async () => ({
      transform: {
        experimentalImportSupport: false,
        inlineRequires: false
      }
    })
  }
  // other metro config, etc
}
