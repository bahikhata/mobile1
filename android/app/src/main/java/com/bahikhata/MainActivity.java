package com.bahikhata;

import android.os.Bundle;
import android.view.View;
import android.graphics.Color;
import com.reactnativenavigation.NavigationActivity;
import androidx.annotation.Nullable;
import android.content.res.Resources;
import com.facebook.react.modules.core.PermissionListener;
import com.imagepicker.permissions.OnImagePickerPermissionsCallback;

import android.widget.LinearLayout;
import android.widget.ImageView;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;

public class MainActivity extends NavigationActivity implements OnImagePickerPermissionsCallback {
  private PermissionListener listener;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState){
    super.onCreate(savedInstanceState);

    Window window = this.getWindow();

    // clear FLAG_TRANSLUCENT_STATUS flag:
    window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

    // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

    // finally change the color
    window.setStatusBarColor(getResources().getColor(R.color.peach));

    //View splash = new View(this);
    LinearLayout splash = new LinearLayout(this);
    splash.setBackgroundColor(getResources().getColor(R.color.peach));
    splash.setGravity(Gravity.CENTER);

    LinearLayout view = new LinearLayout(this);
    ImageView imageView = new ImageView(this);
    imageView.setImageDrawable(getResources().getDrawable(R.drawable.logo,null));

    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(dpToPx(150),dpToPx(150));
    layoutParams.gravity = Gravity.CENTER;
    imageView.setLayoutParams(layoutParams);

    /*TextView textView = new TextView(this);
    textView.setText("Made for India. \n Designed Worldwide.");
    splash.addView(textView);*/
    splash.addView(imageView);

    setContentView(splash);

    getSupportActionBar().hide();
  }

  public static int dpToPx(int dp){
    return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
  }

  @Override
  public void setPermissionListener(PermissionListener listener)
  {
    this.listener = listener;
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
  {
    if (listener != null)
    {
      listener.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
  }

}
