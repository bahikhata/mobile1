const Realm = require('realm');

const TransactionSchema = {
  name: 'Transaction',
  primaryKey: 'id',
  properties: {
    id: 'int',
    account: { type: 'Account' },
    amount: 'double',
    detail: 'string',
    attach: 'string',
    cancelled: { type: 'bool', default: false },
    created: { type: 'date', default: new Date() },
    updated: { type: 'date', default: new Date() }
  }
};

const AccountSchema = {
  name: 'Account',
  primaryKey: 'id',
  properties: {
    id: 'int',
    name: 'string',
    amount: 'double',
    phone: 'string',
    cancelled: { type: 'bool', default: false },
    created: { type: 'date', default: new Date() },
    updated: { type: 'date', default: new Date() }
  }
};

const UserSchema = {
  name: 'User',
  primaryKey: 'id',
  properties: {
    id: 'int',
    name: 'string',
    period: 'date',
    phone: 'string',
    created: { type: 'date', default: new Date() },
    updated: { type: 'date', default: new Date() }
  }
};

const BankSchema = {
  name: 'Bank',
  primaryKey: 'id',
  properties: {
    id: 'int',
    name: 'string',
    detail: 'string',
    date: 'date'
  }
};

const config = {
  schema: [TransactionSchema, AccountSchema, UserSchema, BankSchema],
  schemaVersion: 2
};

const realm = new Realm(config);

module.exports = realm;
