const Sequelize = require('sequelize');

const UserSchema = {
  id: {
    allowNull: false,
    type: Sequelize.UUID,
    primaryKey: true
  },
  name: {
   type: Sequelize.STRING
  },
  period: {
    type: Sequelize.DATE
  },
  phone: {
    type: Sequelize.STRING(15),
    allowNull: false
  },
  syncState: {
     type: Sequelize.INTEGER,
     defaultValue: 1
  }
};

const TransactionSchema = {
  id: {
    allowNull: false,
    type: Sequelize.UUID,
    primaryKey: true
  },
  amount: {
    type: Sequelize.INTEGER,
    defaultValue: 0
  },
  detail: {
    type: Sequelize.TEXT
  },
  media: {
    type: Sequelize.STRING
  },
  cancelled: {
    type: Sequelize.BOOLEAN,
    defaultValue: false
  },
  syncState: {
    type: Sequelize.INTEGER,
    defaultValue: 1
  }
};

const AccountSchema = {
  id: {
    allowNull: false,
    type: Sequelize.UUID,
    primaryKey: true
  },
  name: {
    type: Sequelize.STRING
  },
  phone: {
    type: Sequelize.STRING
  },
  amount: {
    type: Sequelize.DOUBLE,
    defaultValue: 0
  },
  cancelled: {
    type: Sequelize.BOOLEAN,
    defaultValue: false
  },
  syncState: {
    type: Sequelize.INTEGER,
    defaultValue: 1
  }
};

const BankSchema = {
  id: {
    allowNull: false,
    type: Sequelize.UUID,
    primaryKey: true
  },
  name: {
    type: Sequelize.STRING
  },
  detail: {
    type: Sequelize.JSON
  },
  amount: {
    type: Sequelize.DOUBLE
  },
  syncState: {
    type: Sequelize.INTEGER,
    defaultValue: 1
  }
};

module.exports = { UserSchema, TransactionSchema, AccountSchema, BankSchema };
