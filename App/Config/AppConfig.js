// Simple React Native specific changes

export default {
  // font scaling override - RN default is on
  allowTextFontScaling: true,
  mixpanel: 'bb649dc96dd6d33f01e3d836eb8bddc6',
  onesignal: '623871ba-8fe3-469e-a6a9-740ee81bc1fa'
};
