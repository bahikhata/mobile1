import { Text } from 'react-native';
import DebugConfig from './DebugConfig';
import AppConfig from './AppConfig';

module.exports = {
  analytics: AppConfig.mixpanel,
  onesignal: AppConfig.onesignal
};
