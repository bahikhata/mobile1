import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Text,
  View,
  TextInput,
  ScrollView,
  TouchableHighlight,
  FlatList,
  StyleSheet,
  Button,
  Linking
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { ApplicationStyles, Colors, Metrics, Images } from '../Themes'
const I18n = require('../I18n');
import { Navigation } from 'react-native-navigation';

export default class Contact extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerView}>
          <Text style={styles.headerText}>{I18n.t('contact')}</Text>
        </View>
        <View style={styles.border} />
        <TouchableHighlight
          underlayColor={Colors.underlayColor}
          style={styles.buttonView}
          onPress={() => {
            Navigation.dismissOverlay(this.props.componentId);
            Linking.openURL('whatsapp://send?text=hello&phone=+91-7456883325');
          }}
        >
          <Text style={styles.buttonText}>{I18n.t('whatsapp')}</Text>
        </TouchableHighlight>
        <TouchableHighlight
          underlayColor={Colors.underlayColor}
          style={styles.buttonView}
          onPress={() => {
            Navigation.dismissOverlay(this.props.componentId);
            Linking.openURL('tel:+91-7456883325');
          }}
        >
          <Text style={styles.buttonText}>{I18n.t('phone')}</Text>
        </TouchableHighlight>
        <TouchableHighlight
          underlayColor={Colors.underlayColor}
          style={styles.buttonView}
        >
          <Text style={styles.supportText}>{I18n.t('timing')}</Text>
        </TouchableHighlight>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: 220,
    height: 180,
    backgroundColor: Colors.snow,
    borderRadius: 10
  },
  border: {
    width: '100%',
    height: 1,
    backgroundColor: Colors.primary,
    marginBottom: 10
  },
  buttonView: {
    width: '100%',
    padding: 10,
    paddingLeft: 20
  },
  buttonText: {
    color: Colors.black,
    fontSize: 16,
    fontStyle: 'normal'
  },
  supportText: {
    color: Colors.black,
    fontSize: 10,
    fontStyle: 'italic'
  },
  headerView: {
    width: '100%',
    padding: 10,
    alignItems: 'center'
  },
  headerText: {
    color: Colors.primary,
    fontSize: 18
  }
});
