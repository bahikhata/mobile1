import React, { Component } from 'react';
import PropTyps from 'prop-types';
import {
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  TouchableHighlight,
  TouchableWithoutFeedback,
  Linking,
  ImageBackground,
  Modal,
  TextInput,
  ScrollView
} from 'react-native';
import { iconsMap, iconsLoaded } from '../app-icons';
import moment from 'moment';
import { Colors, Metrics, Images, ApplicationStyles, Fonts } from '../Themes';
import Icon from "react-native-vector-icons/FontAwesome";
import { FloatingAction } from "react-native-floating-action";
import { api } from '../Api';

const _ = require("lodash");
import DateTimePicker from '@react-native-community/datetimepicker';
import Mixpanel from '../Components/Analytics';
import { Navigation } from 'react-native-navigation';

const validate = require("validate.js");
const I18n = require('../I18n');
const accounting = require('accounting');
import { dev } from '../../package.json';
import IconButton from "../Components/IconButton";
const Sound = require('react-native-sound');

let account;
var coin;

export default class AccountDetail extends Component {
  constructor(props) {
    super(props);

    this.state = {
      balance: 0,
      txns: [],
      modalVisible: false,
      date: new Date(),
      account: '',
      amount: '',
      amountError: '',
      detail: '',
      phone: ''
    };

    this.onTxnDoneDebounced = _.debounce(this.onTxnDone, 300);
    //this.pickAttachmentDebounced = _.debounce(this.pickAttachment, 300);
    this.showPickerDebounced = _.debounce(this.datePicker, 300);
    this.navigationEventListener = Navigation.events().bindComponent(this);
  }

  componentDidMount() {
    api.getUser().then(user => {
      if(user){
        this.setState({
          period: new Date(user.period)
        });
      }
    });

    coin = new Sound('cash.mp3', Sound.MAIN_BUNDLE, (error) => {
      if(error){
        console.log('Failed to load sound', error);
        return;
      }
    });
  }

  componentWillUnmount() {
    this.onTxnDoneDebounced.cancel();
    this.showPickerDebounced.cancel();
  }

  componentDidAppear(){
    api.getAccountById(this.props.id).then(_account => {
      this.setState({
        account: _account.name,
        balance: _account.amount
      });
      iconsLoaded.then(() => {
        let buttons = [];
        if (_account.phone != "" && _account.phone != null) {
          this.setState({
            phone: _account.phone
          });
          buttons = [
            {
              id: "call",
              icon: iconsMap.phone,
              iconColor: Colors.snow
            },
            {
              id: "whatsapp",
              icon: iconsMap.whatsapp,
              iconColor: Colors.snow
            }
          ];
        }

        Navigation.mergeOptions(this.props.componentId, {
          topBar: {
            title: {
              text: _account.name.substr(0, 25)
            },
            rightButtons: buttons
          }
        });
      });

      this.updateTxns();
    });

    const date = new Date();
    const today = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 23, 59, 59).toISOString();

    api.sumTillDate(new Date().toISOString()).then(result => {
      this.setState({
        closeBalance: result.sum
      });
    });
  }

  navigationButtonPressed({ buttonId }) {
    const { phone } = this.state;
    if(buttonId == 'back'){
      Navigation.pop(this.props.componentId);
    } else if(buttonId == 'call'){
      Linking.openURL(`tel:${phone}`)
    } else if(buttonId == 'whatsapp'){
      let countryCode = "+91";
      let url = `whatsapp://send?text=&phone=${countryCode}${phone}`;
      Linking.canOpenURL(url)
        .then((supported) => {
          if(!supported){
            console.log('not supported');
          } else {
            Linking.openURL(url);
          }
        });
    }
  }

  onTxnDone = (multiplier) => {
    const { account, amount, phone, detail, attachment, date, closeBalance } = this.state;
    const amountUnformatted = accounting.unformat(amount).toString();

    let constraints;
    if (multiplier < 0) {
      constraints = {
        amount: {
          presence: {
            message: I18n.t('required')
          },
          numericality: {
            lessThanOrEqualTo: closeBalance == null ? 0 : closeBalance,
            message: "- Not Enough Cash"
          },
          format: {
            pattern: /^[0-9]+$/,
            message: I18n.t('invalidNo')
          }
        }
      };
    } else {
      constraints = {
        amount: {
          presence: {
            message: I18n.t('required')
          },
          format: {
            pattern: /^[0-9]+$/,
            message: I18n.t('invalidNo')
          }
        }
      };
    }

    const error = validate({ amount: amountUnformatted }, constraints);

    if (!error) {
      api.transaction(account, amountUnformatted, detail, attachment, date.toISOString(), multiplier, this.props.userId).then((result) => {
        if(result){
          coin.play();
          if(!dev)
            Mixpanel.track('Transaction Done');
          this.updateTxns();
          this.dismissModal();
        }
      });
    } else {
      this.setState({
        amountError: error.amount[0]
      });
    }
  }

  datePicker = () => {
    this.setState({
      show: true
    });
  }

  setDate = (event, date) => {
    date = date || this.state.date;

    this.setState({
      show: false,
      date
    });

  }

  newTxn = () => {
    const { account } = this.state;
    if(!dev)
      Mixpanel.track("Transaction Started");
    this.setState({
      modalVisible: true
    });
  }

  updateTxns = () => {
    api.getTxnsByAccountId(this.props.id).then(_txns => {
      this.setState({
        txns: _txns
      });
    });

    api.getAccountById(this.props.id).then(_account => {
      this.setState({
        balance: _account.amount
      });
    });
  }

  viewAttachDetail = (rowId) => {
    Navigation.push(this.props.componentId, {
      component: {
        name: 'screen.attachDetail'
      },
      options: {

      },
      passProps: {
        txn: api.getTxnById(rowId)[0]
      }
    });
  }

  renderItem = (rowData) => {
    let amount = null;
    let detail = null;
    let date = null;
    if(rowData.item.cancelled){
      amount = <Text style={ApplicationStyles.txnAmountCancel}>
        {accounting.formatMoney(Math.abs(rowData.item.amount), '₹', 0)}
      </Text>;
      detail = <Text style={ApplicationStyles.detailCancel}>{rowData.item.detail}</Text>
      date = <Text style={ApplicationStyles.dateRowCancel}>{moment(rowData.item.createdAt).format('DD MMM')}</Text>
    } else {
      amount = <Text style={ApplicationStyles.txnAmount}>
        {accounting.formatMoney(Math.abs(rowData.item.amount), '₹', 0)}
      </Text>;
      detail = <Text style={ApplicationStyles.detail}>{rowData.item.detail}</Text>;
      date = <Text style={ApplicationStyles.dateRow}>{moment(rowData.item.createdAt).format('DD MMM')}</Text>
    }
    return (
        <TouchableHighlight underlayColor={Colors.underlayColor} key={rowData.index}>
          <View style={ApplicationStyles.row}>
              {amount}
              <View style={ApplicationStyles.subrow}>
                  {detail}
                  { rowData.item.media == null ?
                    null :
                    (<TouchableHighlight
                      underlayColor={Colors.underlayColor}
                      onPress={() => this.viewAttachDetail(rowData.item.id)}
                    >
                      <Image
                        style={ApplicationStyles.attach} source={{ uri: rowData.item.attach }}
                      />
                    </TouchableHighlight>)
                  }
                  {date}
              </View>
          </View>
      </TouchableHighlight>
    );
  }

  renderSeparator = () => (
    <View style={ApplicationStyles.separator} />
  )

  dismissModal = () => {
    this.setState({
      modalVisible: false,
      amount: "",
      detail: "",
      amountError: "",
      date: new Date()
    });
  }

  render() {
    const { txns, balance,
      account,
      amount,
      amountError,
      detail,
      attachment,
      hideResults,
      date,
      show,
      period
    } = this.state;
    const option = {
      symbol: '₹',
      decimal: '.',
      thousand: ',',
      precision: 0,
      format: '%s%v'
    };
    const modal = (
      <>
        <TouchableHighlight
          style={styles.spacer}
          onPress={() => this.dismissModal()}
          >
          <Text></Text>
        </TouchableHighlight>
        <ScrollView style={styles.modalView} keyboardShouldPersistTaps='always'>
              { show && <DateTimePicker
                          value={date}
                          minimumDate={period}
                          maximumDate={new Date()}
                          onChange={this.setDate} />
              }
              <View style={styles.buttonGroup}>
                <View style={styles.inButton}>
                  <IconButton
                    iconName='arrow-circle-down'
                    disabled={!amount}
                    text={I18n.t('income')}
                    color={Colors.darkPrimary}
                    onPress={() => this.onTxnDoneDebounced(1)}
                  />
                </View>
                <View style={styles.outButton}>
                  <IconButton
                    iconName='arrow-circle-up'
                    disabled={!amount}
                    text={I18n.t('expense')}
                    color={Colors.darkPrimary}
                    onPress={() => this.onTxnDoneDebounced(-1)}
                  />
                </View>
              </View>
              <View style={styles.form}>
                <View style={styles.formRow}>
                  <View style={styles.subRow}>
                    <TextInput
                        style={styles.amountInput}
                        placeholder={I18n.t('amount')}
                        keyboardType='numeric'
                        spellCheck={false}
                        autoCorrect={false}
                        maxLength={11}
                        autoFocus
                        ref='amount'
                        text={amount}
                        value={amount}
                        selectionColor={Colors.primary}
                        placeholderTextColor={Colors.charcoal}
                        underlineColorAndroid={Colors.transparent}
                        onChangeText={(text) => {
                          //console.log(text);
                          let val;
                          if (text === '₹') {
                            val = '';
                          } else {
                            val = accounting.formatMoney(text, option);
                          }
                          //console.log(val);
                          this.setState({
                            amount: val,
                            amountError: ''
                          });
                        }}
                    />
                    <View style={styles.dateView}>
                      <Icon
                          name='calendar'
                          size={Metrics.icons.small}
                          color={Colors.primary}
                          style={styles.calendarMenu}
                          onPress={() => this.showPicker()}
                      />
                      <Text style={styles.dateText} onPress={() => this.datePicker()}>{moment(date).format('DD MMM')}</Text>
                    </View>
                  </View>
                  {
                    amountError ?
                      <Text style={ApplicationStyles.inputError}>{amountError}</Text>
                      : null
                  }
                  <TextInput
                      style={ApplicationStyles.inputControl}
                      placeholder={I18n.t('detail')}
                      multiline
                      autoCorrect={false}
                      maxLength={60}
                      value={detail}
                      selectionColor={Colors.black}
                      placeholderTextColor={Colors.charcoal}
                      underlineColorAndroid={Colors.transparent}
                      onChangeText={(text) => this.setState({ detail: text })}
                  />
                </View>
              </View>
        </ScrollView>
      </>
    );
    if(!txns){
      return null;
    }
    return (
      <>
        <ImageBackground style={ApplicationStyles.container} source={Images.background} resizeMode='stretch'>
          <View style={ApplicationStyles.accListWrapper}>
            <FlatList
              style={{ flex: 0.5 }}
              data={txns.length > 0 ? txns.filter(txn => txn.amount > 0)
                .sort((a,b) => a.createdAt < b.createdAt) : null}
              renderItem={this.renderItem.bind(this)}
              keyExtractor={item => item.id}
              ItemSeparatorComponent={this.renderSeparator}
              ListHeaderComponent={() => (
                <View style={styles.headerView}>
                  <Icon
                      name='arrow-circle-down'
                      size={Metrics.icons.small}
                      color={Colors.primary}
                      style={styles.icon}
                  />
                  <Text style={styles.headerText}>{I18n.t('receive')}</Text>
                </View>
              )}
              ListFooterComponent={() => (
                <View style={ApplicationStyles.separator} />
              )}
            />
            <FlatList
              style={{ flex: 0.5 }}
              data={txns.length > 0 ? txns.filter(txn => txn.amount < 0)
                .sort((a,b) => a.createdAt < b.createdAt) : null}
              renderItem={this.renderItem.bind(this)}
              keyExtractor={item => item.id}
              ItemSeparatorComponent={this.renderSeparator}
              ListHeaderComponent={() => (
                <View style={styles.headerView}>
                  <Icon
                      name='arrow-circle-up'
                      size={Metrics.icons.small}
                      color={Colors.primary}
                      style={styles.icon}
                  />
                  <Text style={styles.headerText}>{I18n.t('give')}</Text>
                </View>
              )}
              ListFooterComponent={() => (
                <View style={ApplicationStyles.separator} />
              )}
            />
            </View>
            <View style={ApplicationStyles.separator} />
            <Text style={ApplicationStyles.balance}>
              <Text style={{ color: Colors.black, fontSize: 18 }}>{I18n.t('balance')}</Text>
              <Text
                style={ balance >= 0 ? { fontSize: 16, color: Colors.darkGreen } : { fontSize: 16, color: Colors.fire }}
              >
                {accounting.formatMoney(Math.abs(balance), '₹', 0)}
              </Text>
            </Text>
            <Modal
              animationType="slide"
              transparent={true}
              onRequestClose = {() => this.dismissModal()}
              visible={this.state.modalVisible}>
              {modal}
            </Modal>
        </ImageBackground>
        <FloatingAction
          color={Colors.primary}
          onPressMain={this.newTxn}
          showBackground={false}
          position="right"
          distanceToEdge={20}
          floatingIcon={<Icon name="pencil" color={Colors.peach} size={22} />} />
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    resizeMode: 'stretch'
  },
  headerView: {
    flex: 1,
    backgroundColor: Colors.peach,
    flexDirection: 'row',
    alignItems: 'flex-end',
    alignSelf: 'center',
    justifyContent: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 30,
    borderBottomWidth: 0.5,
    borderBottomColor: Colors.primary
  },
  headerText: {
    flex: 0.85,
    paddingLeft: 5,
    fontSize: 16,
    color: Colors.primary,
    fontWeight: 'bold',
  },
  icon: {
    flex: 0.15
  },
  camera: {
    flex: 0.25,
    padding: Metrics.smallMargin,
    marginRight: Metrics.smallMargin,
    marginTop: Metrics.doubleBaseMargin,
    width: Metrics.images.placeholder + 4,
    height: Metrics.images.placeholder + 4,
    borderColor: Colors.primary,
    borderWidth: 0.5,
    alignItems: 'center',
    justifyContent: 'center'
  },
  attachment: {
    width: Metrics.images.placeholder,
    height: Metrics.images.placeholder,
    resizeMode: 'stretch'
  },
  calculatorIcon: {
    flex: 0.1,
    marginTop: 1.5 * Metrics.baseMargin,
    marginRight: Metrics.baseMargin
  },
  attachImage: {
    margin: Metrics.doubleSection
  },
  inButton: {
    flex: 0.5,
    marginLeft: Metrics.smallMargin,
    marginRight: Metrics.smallMargin
  },
  outButton: {
    flex: 0.5,
    marginLeft: Metrics.smallMargin,
    marginRight: Metrics.smallMargin
  },
  buttonGroup: {
    flex: 0.2,
    flexDirection: 'row',
    marginTop: Metrics.baseMargin
  },
  dropdown: {
    padding: Metrics.baseMargin
  },
  spacer: {
    flex: 0.7,
    height: '60%',
    backgroundColor: Colors.underlayColor
  },
  modalView: {
    flex: 1,
    width: '100%',
    resizeMode: 'stretch',
    flex: 0.2, height: '45%',
    alignSelf: 'flex-start',
    backgroundColor: Colors.peach,
    borderColor: Colors.primary,
    borderTopWidth: 1,
    position: 'absolute',
    bottom : 0
  },
  form: {
    flex: 0.1,
    flexDirection: 'row'
  },
  formRow: {
    flex: 1,
    flexDirection: 'column'
  },
  subRow: {
    flex: 1,
    flexDirection: 'row'
  },
  amountInput: {
    flex: 0.7,
    fontSize: Fonts.size.normal,
    margin: Metrics.baseMargin,
    borderBottomColor: Colors.fire,
    borderBottomWidth: 0.5
  },
  dateView: {
    flex: 0.3,
    flexDirection: 'row',
    padding: 10,
    marginTop: 10
  },
  dateText: {
    fontSize: 18,
    paddingLeft: 10
  }
});
