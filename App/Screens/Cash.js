import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet, Text, View, Image,
  DatePickerAndroid, Alert,
  TouchableHighlight, FlatList, Button,
  ImageBackground
} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import Icon from 'react-native-vector-icons/FontAwesome';
import { iconsMap, iconsLoaded } from '../app-icons';
import GestureRecognizer, { swipeDirections } from 'react-native-swipe-gestures';
import * as Animatable from 'react-native-animatable';
import { Colors, Metrics, Images, ApplicationStyles, Fonts } from '../Themes';
import OneSignal from 'react-native-onesignal';
import { onesignal } from "../Config";
import { api } from '../Api';

const _ = require('lodash');
const accounting = require('accounting');
const I18n = require('../I18n');
import { Navigation } from 'react-native-navigation';
import { FloatingAction } from "react-native-floating-action";
import Mixpanel from '../Components/Analytics';
import { dev } from '../../package.json';

export default class Cash extends Component {
  constructor(props) {
    super(props);
    this.state = {
      txns: [],
      date: new Date(),
      selected: -1,
      selectTxn: {},
      show: false,
      period: new Date(),
      userId: ""
    };

    iconsLoaded.then(() => {
        this.initialMenu();
    });

    this.screenState = null;
    this.newTxnDebounced = _.debounce(this.newTxn, 300);
    this.viewAccountDebounced = _.debounce(this.viewAccount, 300);
    this.deleteTxnDebounced = _.debounce(this.deleteTxn, 300);
    this.editTxnDebounced = _.debounce(this.editTxn, 300);

    this.navigationEventListener = Navigation.events().bindComponent(this);
  }

  componentDidMount() {
    if(!dev){
      this.setupNotification();
    }
    this.updateTxns();

    api.getUser().then(user => {
      if(user){
        this.setState({
          period: new Date(user.period),
          userId: user.id
        });
      }
    });
  }

  componentWillUnmount() {
    this.newTxnDebounced.cancel();
    this.viewAccountDebounced.cancel();
    this.deleteTxnDebounced.cancel();
    this.editTxnDebounced.cancel();

    if(!dev){
      OneSignal.removeEventListener('received', this.onReceived);
      OneSignal.removeEventListener('opened', this.onOpened);
      OneSignal.removeEventListener('ids', this.onIds);
    }
  }

  componentDidAppear(){
    this.updateTxns();
  }

  componentDidDisappear(){

  }

  dissmissContextualMenu = () => {
    this.setState({
      selected: -1
    });
    this.initialMenu();
  }

  navigate = (name, title, props) => {
    Navigation.push(this.props.componentId, {
        component: {
          name: name,
          options: {
            topBar: {
              title: {
                text: title
              },
              leftButtons: {
                id: "back",
                icon: iconsMap["arrow-left"],
                iconColor: Colors.snow
              }
            },
            bottomTabs: {
              visible: false,
              drawBehind: true,
            }
          },
          passProps: props
        }
      });
  }

  toggleDrawer = () => {
    Navigation.mergeOptions(this.props.componentId, {
      sideMenu: {
        left: {
          visible: true
        }
      }
    });
  };

  navigationButtonPressed({ buttonId }) {
    if(buttonId == 'sideMenu'){
      this.toggleDrawer();
    } else if(buttonId == 'delete'){
      Alert.alert(
        '',
        I18n.t('deleteTxnMsg'),
        [
          { text: I18n.t('cancel'), onPress: () => this.dissmissContextualMenu() },
          { text: I18n.t('ok'), onPress: () => this.deleteTxnDebounced(this.state.selectTxn) }
        ],
        { cancelable: true }
      );
    } else if (buttonId == "edit"){
      this.dissmissContextualMenu();
      this.editTxnDebounced(this.state.selectTxn);
    } else if (buttonId == "viewAccount"){
      this.dissmissContextualMenu();
      this.viewAccountDebounced(this.state.selectTxn);
    } else if (buttonId == "close"){
      this.dissmissContextualMenu();
    }
  }

  onReceived = (notification) => {
    console.log('Notification received: ', notification);
  }

  onOpened = (openResult) => {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
  }

  onIds = (device) => {
    console.log('Device info: ', device);
  }

  initialMenu = () => {
    Navigation.mergeOptions(this.props.componentId, {
      topBar: {
        visible: true,
        title: {
          text: I18n.t('cashbook')
        },
        background: {
          color: Colors.primary
        },
        leftButtons: {
          id: 'sideMenu',
          icon: iconsMap.bars
        },
        rightButtons: [/*
          {
            id: 'notify',
            icon: iconsMap.bell
          }*/
        ]
      },
      bottomTabs: {
        backgroundColor: Colors.peach
      },
      bottomTab: {
        icon: iconsMap.money,
        iconColor: Colors.primary,
        selectedIconColor: Colors.lightPrimary
      }
    });
  }

  toggleRightDrawer = () => {
    Navigation.mergeOptions(this.props.componentId, {
      sideMenu: {
        right: {
          visible: true
        }
      }
    });
  }

  toggleLeftDrawer = () => {
    Navigation.mergeOptions(this.props.componentId, {
      sideMenu: {
        left: {
          visible: true
        }
      }
    });
  };

  onSwipeLeft = (gestureState) => {
    const today = new Date();
    const newDate = moment(this.state.date).add(1, 'days').toDate();
    if (newDate <= today) {
      this.setState({
        date: newDate
      }, function () {
        this.updateTxns();
      });
    }
    this.refs.txns.bounceInRight();
  }

  onSwipeRight = (gestureState) => {
    const newDate = moment(this.state.date).subtract(1, 'days').toDate();
    this.setState({
        date: newDate
    }, function () {
        this.updateTxns();
        this.refs.txns.bounceInLeft();
    });
  }

  setupNotification = () => {
    OneSignal.init(onesignal);
    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds);
  }

  newTxn = () => {
    if(!dev)
      Mixpanel.track("Transaction Started");
    this.navigate('screen.new', I18n.t('newTxn'), { closeBalance: this.state.closeBalance, userId: this.state.userId });
  }

  showPicker = () => {
    this.setState({
      show: true
    });
  };

  setDate = (event, date) => {
    date = date || this.state.date;

    this.setState({
      show: false,
      date
    });

    if (date != undefined) {
      this.updateTxns();
    }
  }

  updateTxns = () => {
    const date = this.state.date;
    const firstDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0).toISOString();
    const nextDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 23, 59, 59).toISOString();
    api.getTxns(firstDate, nextDate).then(txns => {
      this.setState({
        txns: txns
      });
    });
    api.sumTillDate(firstDate).then(result => {
      this.setState({
        openBalance: result.sum
      });
    });
    api.sumTillDate(nextDate).then(result => {
      this.setState({
        closeBalance: result.sum
      });
    });
  }

  openBalance = () => {
    const { openBalance } = this.state;
    return (
        <View>
          <View style={styles.group}>
            <Text style={styles.openAmount}>
            {accounting.formatMoney(Math.abs(openBalance), '₹', 0)}
            </Text>
            <Text style={styles.openDetail}>{I18n.t('openBalance')}</Text>
          </View>
          <View style={ApplicationStyles.separator} />
        </View>
    );
  }

  closeBalance = () => {
    const { closeBalance, txns } = this.state;
    return (
      <View>
        {
          txns.length > 0 && txns.filter(txn => (txn.amount < 0)).length > 0 ?
          <View style={ApplicationStyles.separator} /> : null
        }
        <View style={styles.group}>
          <Text style={styles.openAmount}>
            {accounting.formatMoney(Math.abs(closeBalance), '₹', 0)}
          </Text>
          <Text style={styles.openDetail}>{I18n.t('closeBalance')}</Text>
        </View>
        <View style={ApplicationStyles.separator} />
      </View>
    );
  }

  longPress = (rowId) => {
    api.getTxnById(rowId).then(txn => {
      this.setState({
        selected: rowId,
        selectTxn: txn
      });

      //Visually show the txn selected
      const buttons = [{
        id: "delete",
        title: I18n.t('delete'),
        icon: iconsMap.trash,
        showAsAction: 'always'
      },
      {
        id: "edit",
        title: I18n.t('editTxn'),
        icon: iconsMap.pencil,
        showAsAction: 'always'
      }];

      if (txn.accountId != null) {
        buttons.push({
          id: "viewAccount",
          title: I18n.t('view'),
          icon: iconsMap.user
        });
      }

      Navigation.mergeOptions(this.props.componentId, {
        topBar: {
            visible: true,
            title: {
              text: ""
            },
            background: {
              color: Colors.darkPrimary
            },
            leftButtons: {
              id: 'close',
              icon: iconsMap.times
            },
            rightButtons: buttons
        }
      });
    });
  }

  viewAttachDetail = (rowId) => {
    api.getTxnById(rowId).then(txn => {
        this.navigate('screen.attachDetail', "", { txn: txn });
    });
  }

  viewAccount = (txn) => {
    this.navigate('screen.accDetail', "", { id: txn.accountId });
  }

  editTxn = (txn) => {
    this.navigate('screen.editTxn', I18n.t('editTxn'), { id: txn.id });
  }

  deleteTxn = (txn) => {
    api.deleteTransaction(txn.id).then((result) => {
      this.dissmissContextualMenu();
      this.updateTxns();
    });
  }

  renderItem = (rowData) => {
    let account = null;
    let detail = null;
    let attach = null;
    let amount = null;

    if (rowData.item.accountId) {
      if(rowData.item.cancelled)
        account = <Text style={ApplicationStyles.accountCancel}>{rowData.item.name}</Text>;
      else
        account = <Text style={ApplicationStyles.account}>{rowData.item.name}</Text>;
    }
    if (rowData.item.detail) {
      if(rowData.item.cancelled)
        detail = <Text style={ApplicationStyles.detailCancel}>{rowData.item.detail}</Text>;
      else
        detail = <Text style={ApplicationStyles.detail}>{rowData.item.detail}</Text>;
    }
    if (rowData.item.media) {
      attach = (
        <TouchableHighlight
          underlayColor={Colors.underlayColor}
          onPress={() => this.viewAttachDetail(rowData.item.id)}
        >
          <Image
            style={ApplicationStyles.attach} source={{ uri: rowData.item.media }}
          />
        </TouchableHighlight>);
    }
    if(rowData.item.cancelled){
      amount = <Text style={ApplicationStyles.txnAmountCancel}>{accounting.formatMoney(Math.abs(rowData.item.amount), '₹', 0)}</Text>;
    } else {
      amount = <Text style={ApplicationStyles.txnAmount}>{accounting.formatMoney(Math.abs(rowData.item.amount), '₹', 0)}</Text>;
    }
    return (
      <TouchableHighlight
        underlayColor={Colors.underlayColor}
        onLongPress={() => !rowData.item.cancelled ? this.longPress(rowData.item.id): null}
      >
        <View
          style={[ApplicationStyles.row,
            this.state.selected === rowData.item.id ?
            { backgroundColor: Colors.lightPrimary } :
            { backgroundColor: 'transparent' }]}
        >
          {amount}
          <View style={ApplicationStyles.subrow}>
              {account}
              {detail}
              {attach}
          </View>
        </View>
      </TouchableHighlight>
    );
  }

  renderSeparator = () => (
      <View style={ApplicationStyles.separator} />
  )

  render() {
    const { txns, date, tipsVisible, show, period } = this.state;
    const config = {
      velocityThreshold: 0.3,
      directionalOffsetThreshold: 80
    };
    return (
      <>
        <GestureRecognizer
          onSwipeLeft={(state) => this.onSwipeLeft(state)}
          onSwipeRight={(state) => this.onSwipeRight(state)}
          config={config}
          style={{ flex: 1 }}
        >
            <View style={styles.dateContainer}>
              <Icon
                  name='calendar'
                  size={Metrics.icons.small}
                  color={Colors.primary}
                  style={styles.calendarMenu}
                  onPress={() => this.showPicker()}
              />
              <Text
                style={styles.date}
                onPress={() => this.showPicker()}
              >
                {moment(date).format('DD MMM')}
              </Text>
              { show && <DateTimePicker
                  value={date}
                  minimumDate={period}
                  maximumDate={new Date()}
                  onChange={this.setDate} />
              }
            </View>
            <Animatable.View
              style={styles.container}
              ref="txns"
            >
              <ImageBackground
                style={styles.imageBackground}
                resizeMode='stretch'
                source={Images.background}
              >
                  <FlatList
                      style={{ flex: 0.5 }}
                      data={txns.length > 0 ? txns.filter(txn => txn.amount > 0)
                        .sort((a,b) => a.createdAt > b.createdAt) : null}
                      renderItem={this.renderItem}
                      keyExtractor={item => item.id.toString()}
                      ItemSeparatorComponent={this.renderSeparator}
                      ListHeaderComponent={this.openBalance}
                      ListFooterComponent={() => (
                          txns.length > 0
                            && txns.filter(txn => txn.amount > 0).length > 0 ?
                              <View style={ApplicationStyles.separator} /> : null
                      )}
                  />
                  <FlatList
                      style={{ flex: 0.5 }}
                      data={txns.length > 0 ? txns.filter(txn => txn.amount < 0)
                        .sort((a,b) => a.createdAt > b.createdAt) : null}
                      renderItem={this.renderItem}
                      keyExtractor={item => item.id.toString()}
                      ItemSeparatorComponent={this.renderSeparator}
                      ListFooterComponent={this.closeBalance}
                  />
            </ImageBackground>
            <FloatingAction
              color={Colors.primary}
              onPressMain={this.newTxn}
              showBackground={false}
              position="right"
              distanceToEdge={20}
              floatingIcon={<Icon name="pencil" color={Colors.peach} size={22} />} />
          </Animatable.View>
        </GestureRecognizer>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    width: '100%',
    resizeMode: 'cover'
  },
  imageBackground: {
    width: '100%',
    flex: 1,
    flexDirection: 'row',
    borderTopWidth: 1,
    borderTopColor: Colors.primary
  },
  openAmount: {
    flex: 0.27,
    fontFamily: 'Hind-Regular',
    color: Colors.charcoal,
    fontSize: Fonts.size.small,
    fontStyle: 'italic',
    textAlignVertical: 'center',
    textAlign: 'right'
  },
  openDetail: {
    flex: 0.73,
    color: Colors.hairline,
    fontFamily: 'Hind-Regular',
    fontSize: Fonts.size.medium,
    textAlignVertical: 'center',
    fontWeight: 'bold',
    marginLeft: Metrics.smallMargin
  },
  group: {
    flexDirection: 'row',
    marginTop: Metrics.baseMargin / 2,
    marginBottom: Metrics.baseMargin / 2
  },
  barsMenu: {
    flex: 0.1,
    textAlignVertical: 'center',
    margin: Metrics.doubleBaseMargin - 2
  },
  calendarMenu: {
    flex: 0.07,
    textAlignVertical: 'center'
  },
  date: {
    flex: 0.25,
    fontSize: Fonts.size.h4,
    color: Colors.primary,
    justifyContent: 'center',
    textAlign: 'center',
    alignSelf: 'center',
    fontStyle: 'italic',
    textAlignVertical: 'center'
  },
  dateContainer: {
    flex: 0.1,
    width: '100%',
    backgroundColor: Colors.peach,
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'center'
  }
});
