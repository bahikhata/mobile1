import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet, Text, View, TouchableOpacity, TextInput, Button, Platform
} from 'react-native';

import { Colors, Metrics, Images, Fonts } from '../Themes';

export default class Notification extends Component {
    constructor(props){
      super(props);

    }

    componentDidMount(){

    }

    render(){
      return(
        <View style={styles.container}>
          <View>
            <Text>Hello</Text>
          </View>
        </View>
      );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: Metrics.drawerWidth,
    backgroundColor: Colors.snow
  }
});
