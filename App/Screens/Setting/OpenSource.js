import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Text,
  View,
  TextInput,
  ScrollView,
  TouchableHighlight,
  FlatList,
  StyleSheet,
  Button,
  Linking
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { ApplicationStyles, Colors, Metrics, Images } from '../../Themes';
import { Navigation } from 'react-native-navigation';

export default class Setting extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };

    this.navigationEventListener = Navigation.events().bindComponent(this);
  }

  navigationButtonPressed({ buttonId }) {
    if(buttonId == 'back'){
      Navigation.pop(this.props.componentId);
    }
  }

  renderItem = (rowData) => {
    return (
      <TouchableHighlight
        style={styles.library}
        underlayColor={Colors.underlayColor}
        onPress={() => Linking.openURL(rowData.item.link)}>
        <View>
          <Text style={styles.name}>{rowData.item.name} - {rowData.item.version}</Text>
          <Text>{rowData.item.description}</Text>
        </View>
      </TouchableHighlight>
    )
  }

  renderSeparator = () => (
      <View style={ApplicationStyles.separator} />
  )

  render() {
    let about = {
      name: 'Open Source',
      description: 'Licenses/Credits for used libraries.'
    }

    let libraries = [
      {
        name: 'ReactJS',
        description: `React is a JavaScript library for building user interfaces.`,
        license: 'MIT',
        version: '16.9.0',
        link: 'https://github.com/facebook/react'
      },
      {
        name: 'React Native',
        description: 'Facebook React Native',
        license: 'MIT',
        version: '0.61.2',
        link: "https://github.com/facebook/react-native"
      },
      {
        name: 'accounting',
        description: `accounting.js is a tiny JavaScript library for number, money and currency parsing/formatting. It's lightweight, fully localisable, has no dependencies, and works great client-side or server-side. Use standalone or as a nodeJS/npm and AMD/requireJS module.`,
        license: '',
        version: '^0.4.1',
        link: 'https://github.com/openexchangerates/accounting.js'
      },
      {
        name: 'lodash',
        description: `The Lodash library exported as Node.js modules.`,
        license: 'MIT',
        version: '^4.17.15',
        link: 'https://github.com/lodash/lodash'
      },
      {
        name: 'moment',
        description: `A lightweight JavaScript date library for parsing, validating, manipulating, and formatting dates.`,
        license: 'MIT',
        version: '^2.24.0',
        link: 'https://github.com/moment/moment'
      },
      {
        name: 'prop-types',
        description: `Runtime type checking for React props and similar objects.`,
        license: 'MIT',
        version: '^15.7.2',
        link: 'https://github.com/facebook/prop-types'
      },
      {
        name: '@types/node',
        description: `This package contains type definitions for Node.js (http://nodejs.org/).`,
        license: 'MIT',
        version: '^12.12.5',
        link: 'https://github.com/DefinitelyTyped/DefinitelyTyped'
      },
      {
        name: '@react-native-community/datetimepicker',
        description: `React Native date & time picker component for iOS and Android`,
        license: 'MIT',
        version: '^2.1.0',
        link: 'https://github.com/react-native-community/react-native-datetimepicker'
      },
      {
        name: 'react-native-animatable',
        description: `Declarative transitions and animations for React Native`,
        license: 'MIT',
        version: '^1.3.3',
        link: 'https://github.com/oblador/react-native-animatable'
      },
      {
        name: 'react-native-autocomplete-input',
        description: `A pure JS autocomplete component for React Native. Use this component in your own projects or use it as inspiration to build your own autocomplete.`,
        license: 'MIT',
        version: '^4.1.0',
        link: 'https://github.com/oblador/react-native-animatable'
      },
      {
        name: 'react-native-contacts',
        description: `Interact with phonebook contacts.`,
        license: 'MIT',
        version: '^5.0.4',
        link: 'https://github.com/rt2zz/react-native-contacts'
      },
      {
        name: 'react-native-floating-action',
        description: `Floating action button for React Native`,
        license: 'MIT',
        version: '^1.19.1',
        link: 'https://github.com/santomegonzalo/react-native-floating-action'
      },
      {
        name: 'react-native-i18n',
        description: `Integrates I18n.js with React Native. Uses the user preferred locale as default.`,
        license: 'MIT',
        version: '^2.0.15',
        link: 'https://github.com/fnando/i18n-js'
      },
      {
        name: 'react-native-image-picker',
        description: `A React Native module that allows you to use native UI to select a photo/video from the device library or directly from the camera, like so:`,
        license: 'MIT',
        version: '^1.1.0',
        link: 'https://github.com/react-native-community/react-native-image-picker'
      },
      {
        name: 'react-native-mixpanel',
        description: `React Native wrapper for Mixpanel library, on top of the regular javascript sdk you can normally use, this provides you all the goodies of the native wrapper including notifications, analysis of the operating system, surveys etc..`,
        license: 'MIT',
        version: '^1.1.4',
        link: 'https://github.com/davodesign84/react-native-mixpanel'
      },
      {
        name: 'react-native-navigation',
        description: `React Native Navigation provides 100% native platform navigation on both iOS and Android for React Native apps. The JavaScript API is simple and cross-platform - just install it in your app and give your users the native feel they deserve. Ready to get started? Check out the docs.`,
        license: 'MIT',
        version: '3.2.0',
        link: 'https://github.com/wix/react-native-navigation'
      },
      {
        name: 'react-native-onesignal',
        description: `OneSignal is a free push notification service for mobile apps. This SDK makes it easy to integrate your native React-Native iOS and/or Android apps with OneSignal.`,
        license: 'MIT',
        version: '^3.4.2',
        link: 'https://github.com/OneSignal/react-native-onesignal'
      },
      {
        name: 'react-native-open-calculator',
        description: `Open native calculator from react native app`,
        license: 'MIT',
        version: '^0.0.4',
        link: 'https://github.com/wix/react-native-navigation'
      },
      {
        name: 'react-native-restart',
        description: `Restart Your React Native Project`,
        license: 'MIT',
        version: '^0.0.13',
        link: 'https://github.com/avishayil/react-native-restart'
      },
      {
        name: 'react-native-shared-preferences',
        description: `Android's Native key value storage system in React Native`,
        license: 'MIT',
        version: '^1.0.1',
        link: 'https://github.com/sriraman/react-native-shared-preferences'
      },
      {
        name: 'react-native-sound',
        description: `React Native module for playing sound clips on iOS, Android, and Windows.`,
        license: 'MIT',
        version: '^0.11.0',
        link: 'https://github.com/zmxv/react-native-sound'
      },
      {
        name: 'react-native-sqlite-storage',
        description: `SQLite3 Native Plugin for React Native for both Android (Classic and Native), iOS and Windows`,
        license: 'MIT',
        version: '^4.1.0',
        link: 'https://github.com/andpor/react-native-sqlite-storage'
      },
      {
        name: 'react-native-swipe-gestures',
        description: `React Native component for handling swipe gestures in up, down, left and right direction.`,
        license: 'MIT',
        version: '^1.0.4',
        link: 'https://github.com/glepur/react-native-swipe-gestures'
      },
      {
        name: 'react-native-vector-icons',
        description: `Perfect for buttons, logos and nav/tab bars. Easy to extend, style and integrate into your project.`,
        license: 'MIT',
        version: '^6.6.0',
        link: 'https://github.com/oblador/react-native-vector-icons'
      },
      {
        name: 'reflect-metadata',
        description: ``,
        license: 'MIT',
        version: '^0.1.13',
        link: 'https://github.com/rbuckton/reflect-metadata'
      },
      {
        name: 'sqlite3',
        description: `Asynchronous, non-blocking SQLite3 bindings for Node.js.`,
        license: 'MIT',
        version: '^4.1.0',
        link: 'https://github.com/mapbox/node-sqlite3'
      },
      {
        name: 'validate.js',
        description: `Validate.js provides a declarative way of validating javascript objects.`,
        license: 'MIT',
        version: '^0.13.1',
        link: 'https://github.com/ansman/validate.js'
      },
    ];

    return (
      <View style={ApplicationStyles.container}>
        <FlatList
            data={libraries}
            renderItem={this.renderItem}
            keyExtractor={item => item.name.toString()}
            ItemSeparatorComponent={this.renderSeparator}
            ListHeaderComponent={() => (
              <View style={styles.header}>
                <Text style={{fontSize: 16, alignSelf: 'center', fontWeight: 'bold'}}>{about.name}</Text>
                <Text>{about.description}</Text>
              </View>
            )}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    textAlign: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    padding: 10
  },
  library: {
    padding: 10
  },
  about: {
    margin: 0
  },
  button: {
    paddingTop: 10,
    paddingLeft: 10,
    paddingRight: 10
  },
  inputControl: {
    margin: 10
  },
  version: {
    paddingTop: 10,
    alignSelf: 'center'
  },
  name: {
    fontSize: 16,
    fontWeight: 'bold'
  },
  libraryVersion: {
    fontSize: 14,
    fontStyle: 'italic'
  }
});
