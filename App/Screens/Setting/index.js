import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Text,
  View,
  TextInput,
  ScrollView,
  TouchableHighlight,
  FlatList,
  StyleSheet,
  Button,
  Linking
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { ApplicationStyles, Colors, Metrics, Images } from '../../Themes';
import { api } from '../../Api';
import { Navigation } from 'react-native-navigation';

import { iconsMap, iconsLoaded } from '../../app-icons';
const SharedPreferences = require('react-native-shared-preferences');
const I18n = require('../../I18n');
import { version } from '../../../package.json';

export default class Setting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      account: ''
    };

    iconsLoaded.then(() => {});

    SharedPreferences.setItem('language', 'en');
    this.navigationEventListener = Navigation.events().bindComponent(this);
  }

  navigationButtonPressed({ buttonId }) {
    if(buttonId == 'back'){
      Navigation.pop(this.props.componentId);
    } else if(buttonId == 'datePick'){
      this.showPickerDebounced({ date: this.state.date, minDate: getUser()[0].period, maxDate: new Date() });
    }
  }

  navigate = (name, title, props) => {
    Navigation.push(this.props.componentId, {
        component: {
          name: name,
          options: {
            topBar: {
              title: {
                text: title
              },
              leftButtons: {
                id: "back",
                icon: iconsMap["arrow-left"],
                iconColor: Colors.snow
              }
            },
            bottomTabs: {
              visible: false,
              drawBehind: true
            }
          },
          passProps: props
        }
      });
  }

  logout = () => {
    api.deleteAll();
  }

  openSource = () => {
    this.navigate("screen.setting.opensource", I18n.t("opensource"), {});
  }

  render() {
    return (
      <View style={ApplicationStyles.container}>
        <View style={styles.button}>
          <Button color={Colors.charcoal} title={I18n.t('language')} onPress={() => {}} />
        </View>
        <View style={styles.button}>
          <Button color={Colors.charcoal} title={I18n.t('opensource')} onPress={() => this.openSource()} />
        </View>
        <View style={styles.button}>
          <Button color={Colors.charcoal} title={I18n.t('legal')} onPress={() => Linking.openURL("http://www.bahikhata.org/legal")} />
        </View>
        <Text style={styles.version}>
          App Version : {version}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    paddingTop: 10,
    paddingLeft: 10,
    paddingRight: 10
  },
  inputControl: {
    margin: 10
  },
  version: {
    paddingTop: 10,
    alignSelf: 'center'
  }
});
