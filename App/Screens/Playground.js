import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet, Text, View, TouchableOpacity, TextInput, Button, Platform
} from 'react-native';
//var PushNotification = require('react-native-push-notification');

/*var Mixpanel = require('react-native-mixpanel');
Mixpanel.sharedInstanceWithToken("c0fd73f57f259ce3889d79fdf1d188e6");
*/

import * as Animatable from 'react-native-animatable';
import Icon from 'react-native-vector-icons/FontAwesome';
import moment from 'moment';
import { ApplicationStyles, Colors, Metrics, Images } from '../Themes';
import DateTimePicker from '@react-native-community/datetimepicker';
import { iconsMap, iconsLoaded } from '../app-icons';

const I18n = require('../I18n');
const validate = require('validate.js');

import { Navigation } from 'react-native-navigation';
import Contacts from "react-native-contacts";
import { api } from '../Api';

const constraints = {
  content: {
    presence: {
      message: 'Required'
    }
  }
};

class Playground extends Component {
  constructor(props) {
    super(props);
    this.state = {
      content: '',
      error: '',
      min: new Date('2019-06-12T14:42:42'),
      max: new Date('2020-06-12T14:42:42'),
      date: new Date(),//new Date('2020-06-12T14:42:42'),
      mode: 'date',
      show: false,
      contacts: []
    };
  }

  componentDidMount() {
    //api.populateDB();

  }
    /*Contacts.getAll((err, contacts) => {
      if (err === "denied") {
        console.warn("Permission to access contacts was denied");
      } else {
        this.setState({ contacts });
      }
    });
    iconsLoaded.then(() => {

    });
  }

  pressed = () => {
    console.log('pressed');
  }

  setDate = (event, date) => {
    date = date || this.state.date;

    this.setState({
      show: Platform.OS === 'ios' ? true : false,
      date,
    });
  }

  show = mode => {
    this.setState({
      show: true,
      mode,
    });
  }

  datepicker = () => {
    this.show('date');
  }

  timepicker = () => {
    this.show('time');
  }

  pickcontact = () => {
    const { contacts } = this.state;
    Navigation.push(this.props.componentId, {
      component: {
        name: "screen.pickContact",
        options: {
          topBar: {
            title: {
              text: "Pick Contact"
            },
            rightButtons: [{
              id: 'search',
              icon: 'search',
              systemItem: 'search',
              enabled: true,
              text: 'Search',
              iconColor: Colors.snow,
              showAsAction: 'always'
            }],
            leftButtons: [{
              id: 'back',
              icon: iconsMap.bars
            }]
          }
        },
        passProps: {
          contacts: contacts
        }
      }
    });
  }


  <View>
    <Button onPress={this.datepicker} title="Show date picker!" />
  </View>
  <View>
    <Button onPress={this.timepicker} title="Show time picker!" />
  </View>
  <View>
    <Button onPress={this.pickcontact} title="Pick Contact" />
  </View>
  { show && <DateTimePicker value={date}
              mode={mode}
              is24Hour={true}
              display="default"
              onChange={this.setDate} />
  }
*/

  fetchTxns = () => {
    const { date, min, max } = this.state;

    api.getTest().then(txns => {
      console.log(txns);
    });

    /*const firstDate = new Date(min.getFullYear(), min.getMonth(), min.getDate(), 0, 0, 0).toISOString();
    const nextDate = new Date(max.getFullYear(), max.getMonth(), max.getDate(), 23, 59, 59).toISOString();

    api.getTxns(firstDate, nextDate).then(txns => {
      console.log(txns);
    });*/
  }

  newTxn = () => {
      const date = this.state.date;

      /*api.test(date.toISOString(), 10).then(result => {
        console.log(result);
      });*/

      api.transaction("Abhijeet",301, "hello details here", null, date.toISOString(),1);
  }

  setDate = (event, date) => {
    date = date || this.state.date;

    this.setState({
      show: false,
      date
    });
  }

  changeDate = () => {
    this.setState({
      show: true
    });
  }

  render() {
    const { show, date, min, max } = this.state;
    return (
      <View style={styles.container}>
        <Text style={{fontSize: 30}}>Hello</Text>
        { show && <DateTimePicker
                    value={new Date()}
                    minimumDate={min}
                    maximumDate={max}
                    onChange={this.setDate} />
        }
        <View>
          <Button onPress={() => this.changeDate()} title="Change Date" />
        </View>

        <View>
          <Button onPress={() => this.newTxn()} title="Add Txn" />
        </View>

        <View>
          <Button onPress={() => this.fetchTxns()} title="Get Txns" />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F5FCFF',
    flex: 1,
    padding: 25
  },
  textInput: {
    borderWidth: 0,
    borderColor: '#fff'
  }
});

/*class PushNotifications extends Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    navigator: PropTypes.object.isRequired
  }
  constructor(props){
    super(props);
    PushNotification.configure({

        // (optional) Called when Token is generated (iOS and Android)
        onRegister: function(token) {
            console.log( 'TOKEN:', token );
        },

        // (required) Called when a remote or local notification is opened or received
        onNotification: function(notification) {
            console.log( 'NOTIFICATION:', notification );
        },

        // ANDROID ONLY: GCM Sender ID (optional - not required for local notifications, but is need to receive remote push notifications)
        senderID: "YOUR GCM SENDER ID",

        // IOS ONLY (optional): default: all - Permissions to register.
        permissions: {
            alert: true,
            badge: true,
            sound: true
        },

        // Should the initial notification be popped automatically
        // default: true
        popInitialNotification: true,

        /**
          * (optional) default: true
          * - Specified if permissions (ios) and token (android and ios) will requested or not,
          * - if not, you must call PushNotificationsHandler.requestPermissions() later
          */
        /*requestPermissions: true,
    });
    Mixpanel.track("Event name");
  }
  myfun(){
    PushNotification.localNotification({
        /* Android Only Properties */
        /*id: 0, // (optional) default: Autogenerated Unique ID
        ticker: "My Notification Ticker", // (optional)
        autoCancel: true, // (optional) default: true
        largeIcon: "ic_launcher", // (optional) default: "ic_launcher"
        smallIcon: "ic_notification", // (optional) default: "ic_notification" with fallback for "ic_launcher"
        bigText: "My big text that will be shown when notification is expanded", // (optional) default: "message" prop
        subText: "This is a subText", // (optional) default: none
        color: "red", // (optional) default: system default
        vibrate: true, // (optional) default: true
        vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000
        tag: 'some_tag', // (optional) add tag to message
        group: "group", // (optional) add group to message
        ongoing: false, // (optional) set whether this is an "ongoing" notification

        /* iOS and Android properties */
      /*  title: "My Notification Title", // (optional, for iOS this is only used in apple watch, the title will be the app name in other devices)
        message: "My Notification Message", // (required)
        playSound: false, // (optional) default: true
        number: 10 // (optional) default: none (Cannot be zero)
    });
    PushNotification.localNotificationSchedule({
      message: "My Notification Message", // (required)
      date: new Date(Date.now() + (5 * 1000)) // in 60 secs
    });

  /*}
  render(){
    return(
      <Text onPress={this.myfun}>Hello</Text>
    );
  }
}*/

module.exports = Playground;
