import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, Button,
  TextInput, ScrollView, Keyboard, PermissionsAndroid
} from 'react-native';
import { iconsMap, iconsLoaded } from '../app-icons';
import Contacts from "react-native-contacts";
import Icon from 'react-native-vector-icons/FontAwesome';
import { Colors, Metrics, ApplicationStyles } from '../Themes';
import { api } from '../Api';
import { Navigation } from 'react-native-navigation';

const _ = require('lodash');
const I18n = require('../I18n');
const validate = require('validate.js');

const constraints = {
  name: {
    presence: {
      message: I18n.t('required')
    }
  }
};

export default class AddAccount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      accountError: '',
      phone: '',
      contacts: []
    };

    this.onPressButtonDebounced = _.debounce(this.onPressButton, 300);
    this.pickContactDebounced = _.debounce(this.pickContact, 300);
    this.navigationEventListener = Navigation.events().bindComponent(this);
  }

  componentDidMount() {
    iconsLoaded.then(() => {
      Navigation.mergeOptions(this.props.componentId, {
        topBar: {
          leftButtons: {
            id: 'back',
            icon: iconsMap["arrow-left"]
          }
        }
      });
    });
  }

  componentDidAppear(){
    _.delay(this.requestContactsPermission.bind(this), 500);
  }

  componentWillUnmount() {
    this.onPressButtonDebounced.cancel();
    this.pickContactDebounced.cancel();
  }

  navigationButtonPressed({ buttonId }) {
    if(buttonId == "back"){
      Keyboard.dismiss();
      Navigation.pop(this.props.componentId);
    }
  }

  async requestContactsPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
        {
          'title': 'Contacts',
          'message': 'This app would like to view your contacts.',
          'buttonPositive': 'Please accept bare mortal'
        }
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        /*SmsAndroid.list(JSON.stringify(filter), (fail) => {
            console.log("OH Snap: " + fail);
        }, (count, smsList) => {
            this.setState({
              txns: JSON.parse(smsList)
            });
            /*for (let i = 0; i < this.state.txns.length; i++) {
                this.parseSMS(this.state.txns[i]);
            }
        });*/
        Contacts.getAll((err, contacts) => {
          if (err === "denied") {
            console.log("Error Accessing contacts");
          } else {
            this.setState({ contacts });
          }
        });
      } else {
        console.log("Permission to access contacts was denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }

  onPressButton = () => {
    const { name, phone } = this.state;
    const error = validate({ name }, constraints);
    if (!error) {
      api.getAccountByName(name).then(_account => {
        if (_account == null) {
          api.account(name, phone, this.props.userId).then(result => {
            if(result == true){
              Keyboard.dismiss();
              Navigation.pop(this.props.componentId);
            }
          });
        } else {
          this.setState({
            accountError: I18n.t('accountExists')
          });
        }
      });
    } else {
      this.setState({
        accountError: error.account ? error.account[0] : ''
      });
    }
  }

  pickContact = () => {
    /*SelectContacts.pickContact({ timeout: 45000 }, (err, contact) => {
      if (err) {
        console.log(err.message);
      } else if (api.getAccountByName(contact.name).length === 0) {
        this.setState({
          account: contact.name,
          phone: contact.phoneNumbers ? contact.phoneNumbers[0].number : ''
        });
      } else {
        this.setState({
          accountError: I18n.t('accountExists')
        });
      }
    });*/
    const { contacts } = this.state;
    Navigation.push(this.props.componentId, {
      component: {
        name: "screen.pickContact",
        options: {
          topBar: {
            title: {
              text: "Pick Contact"
            },
            rightButtons: {
              id: 'search',
              icon: 'search',
              systemItem: 'search',
              enabled: true,
              text: 'Search',
              iconColor: Colors.snow
            },
            leftButtons: {
              id: 'back',
              icon: iconsMap.bars
            }
          },
          bottomTabs: {
            visible: false,
            drawBehind: true
          }
        },
        passProps: {
          contacts: contacts
        }
      }
    });
  }

  /*
  <Icon
    name='user'
    size={Metrics.icons.medium}
    color={Colors.primary}
    style={ApplicationStyles.contactIcon}
    onPress={this.pickContactDebounced}
  />
  */

  render() {
    const { name, accountError, phone } = this.state;
    return (
      <ScrollView style={ApplicationStyles.container} keyboardShouldPersistTaps='never'>
          <View style={ApplicationStyles.miniForm}>
            <TextInput
              style={ApplicationStyles.inputControl}
                placeholder={I18n.t('account')}
                ref='account'
                value={name}
                maxLength={35}
                autoFocus
                selectionColor={Colors.black}
                placeholderTextColor={Colors.charcoal}
                underlineColorAndroid={Colors.transparent}
                onChangeText={(text) => this.setState({ name: text, accountError: '' })}
            />
          </View>
          {
            accountError ?
            <Text style={ApplicationStyles.inputError}>
              {accountError}
            </Text> : null
          }
          <View style={ApplicationStyles.miniForm}>
            <TextInput
              style={ApplicationStyles.inputControl}
                placeholder={I18n.t('phone')}
                ref='account'
                value={phone}
                maxLength={10}
                keyboardType='numeric'
                selectionColor={Colors.black}
                placeholderTextColor={Colors.charcoal}
                underlineColorAndroid={Colors.transparent}
                onChangeText={(text) => this.setState({ phone: text, accountError: '' })}
            />
          </View>
          <View style={ApplicationStyles.buttonWrapper}>
              <Button
                title={I18n.t('ok')}
                color={Colors.primary}
                onPress={this.onPressButtonDebounced}
              />
          </View>
      </ScrollView>
    );
  }
}
