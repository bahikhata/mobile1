import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Text,
  View,
  TextInput,
  ScrollView,
  TouchableHighlight,
  FlatList,
  StyleSheet,
  Button,
  Linking,
  Share
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { ApplicationStyles, Colors, Metrics, Images } from '../Themes'
const I18n = require('../I18n');
import { Navigation } from 'react-native-navigation';
const accounting = require("accounting");
import { api } from '../Api';
import { iconsMap, iconsLoaded } from '../app-icons';

export default class CashTransfer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      amount: 0,
      amount1: 0,
      code: null,
      otp: null,
      message: null
    };

    iconsLoaded.then(() => {});
    this.navigationEventListener = Navigation.events().bindComponent(this);
  }

  navigationButtonPressed({ buttonId }) {
    if(buttonId == 'back'){
      Navigation.pop(this.props.componentId);
    }
  }

  sendMoney = () => {
    const { amount } = this.state;
    const code = Math.floor(Math.random() * 99999);
    this.setState({
      code: code
    });
    api.send(amount, code);
  }

  receiveMoney = () => {
    const { amount1,otp1 } = this.state;

    api.receive(amount1, otp1).then(result => {
      this.setState({
        message: result
      });
    });
  }

  shareCode = () => {
    const { code } = this.state;
    Linking.openURL(`whatsapp://send?text=${code}`);
  }

  render() {
    const { amount, amount1, code, otp, otp1, message } = this.state;
    const option = {
      symbol: '₹',
      decimal: '.',
      thousand: ',',
      precision: 0,
      format: '%s%v'
    };
    return (
      <ScrollView style={styles.container}>
        <View style={styles.subRow}>
          <Text>Send</Text>
          <TextInput
              style={ApplicationStyles.inputControl}
              placeholder={I18n.t('amount')}
              autoCorrect={false}
              keyboardType='numeric'
              maxLength={11}
              text={amount}
              value={amount}
              selectionColor={Colors.black}
              placeholderTextColor={Colors.charcoal}
              underlineColorAndroid={Colors.transparent}
              onChangeText={(text) => {
                //console.log(text);
                let val;
                if (text === '₹') {
                  val = '';
                } else {
                  val = accounting.formatMoney(text, option);
                }
                //console.log(val);
                this.setState({
                  amount: val
                });
              }}
          />
        </View>
        <View style={styles.subRow}>
          <Button title="Send" onPress={this.sendMoney} color={Colors.primary} />
        </View>
        <View style={styles.message}>
          <Text style={styles.code}>{code}</Text>
          <Icon
              style={styles.shareIcon}
              name="share-alt"
              size={Metrics.icons.medium}
              color={Colors.black}
              onPress={this.shareCode}
          />
        </View>
        <View style={styles.subRow}>
          <Text>Receive</Text>
          <TextInput
              style={ApplicationStyles.inputControl}
              placeholder={I18n.t('amount')}
              autoCorrect={false}
              keyboardType='numeric'
              maxLength={11}
              text={amount1}
              value={amount1}
              selectionColor={Colors.black}
              placeholderTextColor={Colors.charcoal}
              underlineColorAndroid={Colors.transparent}
              onChangeText={(text) => {
                //console.log(text);
                let val;
                if (text === '₹') {
                  val = '';
                } else {
                  val = accounting.formatMoney(text, option);
                }
                //console.log(val);
                this.setState({
                  amount1: val
                });
              }}
          />
          <TextInput
              style={ApplicationStyles.inputControl}
              placeholder={I18n.t('otp')}
              autoCorrect={false}
              keyboardType='numeric'
              maxLength={11}
              text={otp1}
              value={otp1}
              selectionColor={Colors.black}
              placeholderTextColor={Colors.charcoal}
              underlineColorAndroid={Colors.transparent}
              onChangeText={(text) => {
                this.setState({
                  otp1: text
                });
              }}
          />
        </View>
        <View style={styles.subRow}>
          <Button title="Accept" onPress={this.receiveMoney} color={Colors.primary} />
        </View>
        <View style={styles.message}>
          <Text style={styles.code}>{message}</Text>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderWidth: 1
  },
  subRow: {
    flex: 0.1,
    padding: 5
  },
  message: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: Colors.lightPrimary,
    padding: 10,
    margin: 10,
    borderWidth: 1
  },
  code: {
    flex: 0.9,
    fontSize: 18,
    alignSelf: 'center'
  },
  shareIcon: {
    flex: 0.1
  }
});
