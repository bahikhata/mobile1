import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  StyleSheet,
  Text,
  View,
  ToastAndroid,
  Image,
  TextInput,
  ScrollView,
  Button,
  TouchableHighlight,
  Platform,
  TouchableOpacity,
  Keyboard
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import Calculator from "react-native-open-calculator";
import DateTimePicker from '@react-native-community/datetimepicker';
import { iconsMap, iconsLoaded } from '../app-icons';
import moment from "moment";
//import SelectContacts from "react-native-select-contact-android";
import ImagePicker from "react-native-image-picker";
import { Colors, Metrics, ApplicationStyles, Fonts } from "../Themes";
//import Autocomplete from "../Components/Autocomplete";
import IconButton from "../Components/IconButton";
import { api } from "../Api";
import Autocomplete from "react-native-autocomplete-input";

const _ = require("lodash");
const I18n = require("../I18n");
const validate = require("validate.js");
const Mixpanel = require("../Components/Analytics");
const accounting = require("accounting");
const Sound = require('react-native-sound');

import { FloatingAction } from "react-native-floating-action";
import { Navigation } from 'react-native-navigation';
import { dev } from '../../package.json';

const options = {
  title: I18n.t('attach'),
  storageOptions: {
    skipBackup: true,
    path: 'images'
  }
};
var coin;

export default class New extends Component {
  constructor(props) {
    super(props);

    this.state = {
      amount: '',
      amountError: '',
      detail: '',
      account: '',
      phone: '',
      show: false,
      date: new Date(),
      minDate: null,
      attachment: '',
      hideResults: false
    };

    this.onTxnDoneDebounced = _.debounce(this.onTxnDone, 300);
    this.pickContactDebounced = _.debounce(this.pickContact, 300);
    this.pickAttachmentDebounced = _.debounce(this.pickAttachment, 300);
    this.showPickerDebounced = _.debounce(this.datePicker, 300);

    this.navigationEventListener = Navigation.events().bindComponent(this);
  }

  componentDidMount() {
    //Get the minimum date for selection.
    api.getUser().then(user => {
      if(user){
        this.setState({
          period: new Date(user.period)
        });
      }
    });

    //Prefill the accounts for account autocomplete dropdown.
    api.getAccounts().then(_accounts => {
      const accounts = _accounts.map(account => account.name);
      this.setState({
        data: accounts
      });
    });

    //Set Date Picker text in the top right menu.
    iconsLoaded.then(() => {

    });

    coin = new Sound('cash.mp3', Sound.MAIN_BUNDLE, (error) => {
      if(error){
        console.log('Failed to load sound', error);
        return;
      }
    });
  }

  componentWillUnmount() {
    this.onTxnDoneDebounced.cancel();
    this.pickContactDebounced.cancel();
    this.pickAttachmentDebounced.cancel();
    this.showPickerDebounced.cancel();
  }

  navigationButtonPressed({ buttonId }) {
    if(buttonId == 'back'){
      Keyboard.dismiss();
      Navigation.pop(this.props.componentId);
    } else if(buttonId == 'datePick'){
      this.showPickerDebounced();
    }
  }

  onTxnDone = (multiplier) => {
    const { account, amount, phone, detail, attachment, date } = this.state;
    const { closeBalance } = this.props;
    const amountUnformatted = accounting.unformat(amount).toString();

    const lastDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 23, 59, 59).toISOString();

    let constraints;
    if (multiplier < 0) {
      console.log(multiplier + " multiplier " + amountUnformatted + "  " + closeBalance);
      constraints = {
        amount: {
          presence: {
            message: I18n.t('required')
          },
          numericality: {
            lessThanOrEqualTo: closeBalance == null ? 0 : closeBalance,
            message: "- Low Balance"
          },
          format: {
            pattern: /^[0-9]+$/,
            message: I18n.t('invalidNo')
          }
        }
      };
    } else {
      constraints = {
        amount: {
          presence: {
            message: I18n.t('required')
          },
          format: {
            pattern: /^[0-9]+$/,
            message: I18n.t('invalidNo')
          }
        }
      };
    }

    const error = validate({ amount: amountUnformatted }, constraints);

    if (!error) {
      api.transaction(account, amountUnformatted, detail, attachment, date.toISOString(), multiplier, this.props.userId).then((result) => {
        if(result){
          coin.play();
          if(!dev)
            Mixpanel.track('Transaction Done');
          Keyboard.dismiss();
          Navigation.pop(this.props.componentId);
        }
      });
    } else {
      this.setState({
        amountError: error.amount[0]
      });
    }
  }

  datePicker = () => {
    this.setState({
      show: true
    });
  }

  setDate = (event, date) => {
    date = date || this.state.date;

    this.setState({
      show: false,
      date
    });

  }

  openCalculator = () => {
    if (Platform.OS === 'android') {
        Calculator.open();
    }
  }

  pickContact = () => {
    /*SelectContacts.pickContact({ timeout: 45000 }, (err, contact) => {
      if (err) {
        alert(err.message);
      }
      api.getAccountByName(contact.name).then(account => {
        if(account.length === 0) {
          this.setState({
            account: contact.name,
            phone: contact.phoneNumbers ? contact.phoneNumbers[0].number : ''
          });
        } else {
          ToastAndroid.show('Account already exists', ToastAndroid.SHORT);
        }
      });
    });*/
  }

  pickAttachment = () => {
    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        const source = { uri: response.uri };
        this.setState({
          attachment: source
        });
      }
    });
  }

  filterData = (query) => {
    if (query === '') {
      return [];
    }
    const data = this.state.data;
    const regex = new RegExp(`${query.trim()}`, 'i');
    return data.filter(dat => dat.search(regex) >= 0);
  }

  renderDropDown = (value, i) => {
    return (
      <TouchableOpacity
          underlayColor={Colors.underlayColor}
          style={styles.dropdown}
          onPress={() => this.setState({ account: value.item, hideResults: true })}
        >
          <Text>
            {value.item}
          </Text>
        </TouchableOpacity>
    );
  }

  /* <TouchableHighlight
      underlayColor={Colors.underlayColor}
      style={ApplicationStyles.contactIcon}
      onPress={this.pickContactDebounced}
  >
    <Icon
      name='user'
      size={Metrics.icons.medium}
      color={Colors.black}
    />
  </TouchableHighlight>


  {attach}*/

  render() {
    const {
      account,
      amount,
      amountError,
      detail,
      attachment,
      hideResults,
      date,
      show,
      period
    } = this.state;
    const data = this.filterData(account);
    const option = {
      symbol: '₹',
      decimal: '.',
      thousand: ',',
      precision: 0,
      format: '%s%v'
    };
    const attach = (
      <TouchableHighlight
        underlayColor={Colors.underlayColor}
        style={styles.camera}
        onPress={this.pickAttachmentDebounced}
      >
        {
          attachment === '' ?
            <Icon
                name='camera'
                size={Metrics.icons.medium}
                color={Colors.black}
            /> :
            <Image
                style={styles.attachment}
                source={attachment}
            />
        }
      </TouchableHighlight>
    );
    return (
      <>
        <ScrollView style={ApplicationStyles.container} keyboardShouldPersistTaps='always'>
              { show && <DateTimePicker
                          value={date}
                          minimumDate={period}
                          maximumDate={new Date()}
                          onChange={this.setDate} />
              }
              <View style={styles.form}>
                <View style={styles.formRow}>
                  <View style={styles.subRow}>
                    <Autocomplete
                        style={ApplicationStyles.inputControl}
                        inputContainerStyle={{borderWidth: 0}}
                        listStyle={{borderColor: Colors.primary}}
                        keyboardShouldPersistTaps='always'
                        data={data}
                        autoFocus
                        placeholder={I18n.t('account')}
                        maxLength={35}
                        keyExtractor={item => item.id}
                        hideResults={hideResults}
                        autoCorrect={false}
                        spellCheck={false}
                        defaultValue={account}
                        selectionColor={Colors.black}
                        placeholderTextColor={Colors.charcoal}
                        underlineColorAndroid={Colors.transparent}
                        onChangeText={text => this.setState({ account: text, hideResults: false })}
                        renderItem={this.renderDropDown}
                    />
                  </View>
                  <View style={styles.subRow}>
                    <TextInput
                        style={ApplicationStyles.inputControl}
                        placeholder={I18n.t('amount')}
                        keyboardType='numeric'
                        spellCheck={false}
                        autoCorrect={false}
                        maxLength={11}
                        ref='amount'
                        text={amount}
                        value={amount}
                        selectionColor={Colors.primary}
                        placeholderTextColor={Colors.charcoal}
                        underlineColorAndroid={Colors.transparent}
                        onChangeText={(text) => {
                          //console.log(text);
                          let val;
                          if (text === '₹') {
                            val = '';
                          } else {
                            val = accounting.formatMoney(text, option);
                          }
                          //console.log(val);
                          this.setState({
                            amount: val,
                            amountError: ''
                          });
                        }}
                    />
                    <View style={styles.dateView}>
                      <Icon
                          name='calendar'
                          size={Metrics.icons.small}
                          color={Colors.primary}
                          style={styles.calendarMenu}
                          onPress={() => this.showPicker()}
                      />
                      <Text style={styles.dateText} onPress={() => this.datePicker()}>{moment(date).format('DD MMM')}</Text>
                    </View>
                  </View>
                  {
                    amountError ?
                      <Text style={ApplicationStyles.inputError}>{amountError}</Text>
                      : null
                  }
                  <TextInput
                      style={ApplicationStyles.inputControl}
                      placeholder={I18n.t('detail')}
                      multiline
                      autoCorrect={false}
                      maxLength={60}
                      value={detail}
                      selectionColor={Colors.black}
                      placeholderTextColor={Colors.charcoal}
                      underlineColorAndroid={Colors.transparent}
                      onChangeText={(text) => this.setState({ detail: text })}
                  />
                </View>

              </View>
              <View style={styles.buttonGroup}>
                <View style={styles.inButton}>
                  <IconButton
                    iconName='arrow-circle-down'
                    disabled={!amount}
                    text={I18n.t('income')}
                    color={Colors.darkPrimary}
                    onPress={() => this.onTxnDoneDebounced(1)}
                  />
                </View>
                <View style={styles.outButton}>
                  <IconButton
                    iconName='arrow-circle-up'
                    disabled={!amount}
                    text={I18n.t('expense')}
                    color={Colors.darkPrimary}
                    onPress={() => this.onTxnDoneDebounced(-1)}
                  />
                </View>
              </View>
          </ScrollView>
          <FloatingAction
            color={Colors.primary}
            onPressMain={this.openCalculator}
            showBackground={false}
            position="right"
            distanceToEdge= {15}
            floatingIcon={<Icon name="calculator" color={Colors.peach} size={22} />} />
        </>
    );
  }
}

let styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.peach,
    width: '100%'
  },
  camera: {
    flex: 0.25,
    padding: Metrics.smallMargin,
    marginRight: Metrics.smallMargin,
    marginTop: Metrics.doubleBaseMargin,
    width: Metrics.images.placeholder + 4,
    height: Metrics.images.placeholder + 4,
    borderColor: Colors.primary,
    borderWidth: 0.5,
    alignItems: 'center',
    justifyContent: 'center'
  },
  attachment: {
    width: Metrics.images.placeholder,
    height: Metrics.images.placeholder,
    resizeMode: 'stretch'
  },
  calculatorIcon: {
    flex: 0.1,
    marginTop: 1.5 * Metrics.baseMargin,
    marginRight: Metrics.baseMargin
  },
  attachImage: {
    margin: Metrics.doubleSection
  },
  inButton: {
    flex: 0.5,
    marginRight: Metrics.smallMargin,
    marginLeft: Metrics.baseMargin
  },
  outButton: {
    flex: 0.5,
    marginLeft: Metrics.smallMargin5,
    marginRight: Metrics.baseMargin
  },
  buttonGroup: {
    flex: 0.2,
    flexDirection: 'row',
    marginTop: Metrics.baseMargin
  },
  dropdown: {
    padding: Metrics.baseMargin,
    backgroundColor: Colors.peach
  },
  subRow: {
    flex: 1,
    flexDirection: 'row'
  },
  form: {
    flex: 1,
    flexDirection: 'row'
  },
  formRow: {
    flex: 1,
    flexDirection: 'column'
  },
  amountInput: {
    flex: 0.6,
    fontSize: Fonts.size.normal,
    margin: Metrics.baseMargin,
    borderBottomColor: Colors.fire,
    borderBottomWidth: 0.5
  },
  dateView: {
    flex: 0.4,
    flexDirection: 'row',
    padding: 10,
    marginTop: 10
  },
  dateText: {
    fontSize: 18,
    paddingLeft: 10,
    color: Colors.hairline,
    fontWeight: "bold"
  }
});
