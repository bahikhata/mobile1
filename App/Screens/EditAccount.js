import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, Button, TextInput, ScrollView, ToastAndroid, Keyboard } from 'react-native';
import { Colors, Metrics, Images, ApplicationStyles } from '../Themes';
import { api } from '../Api';
import { Navigation } from 'react-native-navigation';

const _ = require('lodash');
const I18n = require('../I18n');
const validate = require('validate.js');

const constraints = {
  account: {
    presence: {
      message: I18n.t('required')
    }
  }
};

export default class EditAccount extends Component {
  static propTypes = {
    id: PropTypes.string.isRequired
  }
  constructor(props) {
    super(props);
    this.state = {
      account: '',
      accountError: ''
    };
    this.onPressButtonDebounced = _.debounce(this.onPressButton, 300);
    this.navigationEventListener = Navigation.events().bindComponent(this);
  }

  componentDidMount() {
    const { id } = this.props;
    
    api.getAccountById(id).then(result => {
      this.setState({
        account: result.name
      });
    });
  }

  componentWillUnmount() {
      this.onPressButtonDebounced.cancel();
  }

  navigationButtonPressed({ buttonId }) {
    if(buttonId == 'back'){
      Keyboard.dismiss();
      Navigation.pop(this.props.componentId);
    }
  }

  onPressButton = () => {
    const { account } = this.state;
    const error = validate({ account }, constraints);
    if (!error) {
      api.getAccountByName(account).then(result => {
        if(result != null){
          this.setState({
            accountError: I18n.t('accountExists')
          });
        } else {
          api.updateAccount(this.props.id, {name: account }, new Date().toISOString()).then(result => {
            console.log(result);
            if(result) {
              Keyboard.dismiss();
              Navigation.pop(this.props.componentId);
            }
          });
        }
      });
    } else {
      this.setState({
        accountError: error.account[0]
      });
    }
  }

  render() {
    const { account, accountError } = this.state;
    return (
      <ScrollView style={ApplicationStyles.container} keyboardShouldPersistTaps='always'>
          <TextInput
            style={ApplicationStyles.inputControl}
              placeholder={I18n.t('account')}
              ref='account'
              value={account}
              maxLength={35}
              autoFocus
              selectionColor={Colors.black}
              placeholderTextColor={Colors.charcoal}
              underlineColorAndroid={Colors.transparent}
              onChangeText={(text) => this.setState({ account: text, accountError: '' })}
          />
          {
            accountError ?
            <Text style={ApplicationStyles.inputError}>
              {accountError}
            </Text> : null
          }
          <View style={ApplicationStyles.buttonWrapper}>
              <Button
                title={I18n.t('ok')}
                color={Colors.primary}
                onPress={this.onPressButtonDebounced}
              />
          </View>
      </ScrollView>
    );
  }
}
