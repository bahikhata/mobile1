import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Text, View, TextInput, ScrollView, StyleSheet, Button, Image, RNDateTimePicker, TouchableOpacity
} from 'react-native';
import RNRestart from 'react-native-restart';
import Icon from 'react-native-vector-icons/FontAwesome';
import { ApplicationStyles, Colors, Metrics, Images } from '../Themes';
import { api } from '../Api';
import moment from 'moment';

const _ = require('lodash');
const validate = require('validate.js');
const I18n = require('../I18n');
const Mixpanel = require('../Components/Analytics');
import DateTimePicker from '@react-native-community/datetimepicker';
import { Navigation } from 'react-native-navigation';
import { dev } from '../../package.json';

import { iconsMap, iconsLoaded } from '../app-icons';

const constraints = {
  name: {
    presence: {
      message: I18n.t('required')
    }
  }
};

export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: null,
      nameError: '',
      date: new Date(2019, 3, 1), //#Todo: Instead of hardcoding as of now... Make it Dynamic.
      show: false
    };

    this.startAppDebounced = _.debounce(this.startApp, 300);
    this.datePickerDebounced = _.debounce(this.datePicker, 300);
  }

  componentDidMount() {
    api.populateDB().then(result => {
      console.log(result);
    });
  }

  componentWillUnmount() {
    this.startAppDebounced.cancel();
    this.datePickerDebounced.cancel();
  }

  startApp = () => {
    const { name, date } = this.state;
    const error = validate({ name }, constraints);
    if (!error) {
      api.user(name, date.toISOString(), "").then((result) => {
        api.getUser().then(user => {
            if(!dev){
              Mixpanel.identify(user.id);
              Mixpanel.set({ $name: name });
            }
            RNRestart.Restart();
        });
      });


      //If previous account exists then sync the previous data else show main screen.
    } else {
      this.setState({
        nameError: error.name[0]
      });
    }
  }

  datePicker = () => {
    this.setState({
      show: true
    });
  }

  setDate = (event, date) => {
    date = date || this.state.date;

    this.setState({
      show: false,
      date
    });
  }

  render() {
    const { name, nameError, date, show } = this.state;
    return (
      <ScrollView style={ApplicationStyles.container} keyboardShouldPersistTaps='always'>
        <View style={styles.view}>
          <TextInput
              style={ApplicationStyles.inputControl}
              placeholder={I18n.t('business')}
              multiline={false}
              autoCorrect={false}
              autoFocus
              maxLength={30}
              value={name}
              selectionColor={Colors.black}
              placeholderTextColor={Colors.charcoal}
              underlineColorAndroid={Colors.transparent}
              onChangeText={(text) => this.setState({ name: text, nameError: '' })}
          />
          {
            nameError ?
            <Text style={ApplicationStyles.inputError}>{nameError}</Text>
            : null
          }
        </View>
        <View
          style={[styles.view,
          { marginLeft: Metrics.doubleBaseMargin,
            marginTop: Metrics.baseMargin,
            marginBottom: Metrics.baseMargin }]}
        >
          <Text style={styles.textView}>{I18n.t('yearStart')}</Text>
          <View
            style={[ApplicationStyles.miniForm,
            { marginLeft: 0,
              marginTop: 0,
              marginBottom: 10,
              paddingTop: 0
            }]}
          >
            <TouchableOpacity
              style={{flex: 0.9}}
              onPress={this.datePickerDebounced}>
              <TextInput
                  style={ApplicationStyles.inputControl}
                  placeholder={I18n.t('yearStart')}
                  multiline={false}
                  autoCorrect={false}
                  maxLength={25}
                  editable={false}
                  value={moment(date).format('DD MMM YYYY')}
                  selectionColor={Colors.black}
                  placeholderTextColor={Colors.charcoal}
                  underlineColorAndroid={Colors.transparent}
                  onChangeText={(text) => this.setState({ date: text })}
              />
            </TouchableOpacity>
            <Icon
              name='calendar'
              size={Metrics.icons.medium}
              color={Colors.primary}
              style={ApplicationStyles.contactIcon}
              onPress={this.datePickerDebounced}
            />
          </View>
        </View>
        { show && <DateTimePicker value={date}
                    mode={'date'}
                    is24Hour={true}
                    display="default"
                    onChange={this.setDate} />
        }
        <View style={styles.view}>
          <Button
            title={I18n.t('start')}
            color={Colors.darkPrimary}
            onPress={this.startAppDebounced}
          />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  imageView: {
    margin: Metrics.doubleSection,
    alignItems: 'center',
    alignContent: 'center'
  },
  image: {
    width: Metrics.images.placeholder,
    height: Metrics.images.placeholder,
    resizeMode: 'contain'
  },
  view: {
    margin: Metrics.baseMargin,
    marginTop: 0,
    marginBottom: 0
  },
  textView: {
    color: Colors.charcoal,
    fontSize: 14,
    fontStyle: 'italic'
  }
});
