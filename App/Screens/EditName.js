import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollView, StyleSheet, View, Text,
  Dimensions, Button, TextInput, DatePickerAndroid,
  TouchableOpacity, Keyboard } from 'react-native';
import { Colors, Metrics, Images, ApplicationStyles } from '../Themes';
import { api } from '../Api';
import Icon from 'react-native-vector-icons/FontAwesome';
import moment from 'moment';
import { Navigation } from 'react-native-navigation';
import DateTimePicker from '@react-native-community/datetimepicker';

const _ = require('lodash');
const validate = require('validate.js');
const I18n = require('../I18n');

const constraints = {
  name: {
    presence: {
      message: I18n.t('required')
    }
  }
};

export default class EditName extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: null,
      nameError: '',
      show: false
    };

    this.onPressButtonDebounced = _.debounce(this.onPressButton, 300);
    this.datePickerDebounced = _.debounce(this.datePicker, 300);
    this.navigationEventListener = Navigation.events().bindComponent(this);
  }

  componentDidMount() {
    api.getUser().then(user => {
      if(user){
        this.setState({
          id: user.id,
          name: user.name,
          date: new Date(user.period)
        });
      }
    });
  }

  componentWillUnmount() {
    this.onPressButtonDebounced.cancel();
  }

  navigationButtonPressed({ buttonId }) {
    if (buttonId == "back") {
      Keyboard.dismiss();
      Navigation.pop(this.props.componentId);
    }
  }

  onPressButton = () => {
    const { id, name, date } = this.state;
    const error = validate({ name }, constraints);
    if (!error) {
      api.updateUser(id, name, date.toISOString()).then(result => {
        if (result) {
          Keyboard.dismiss();
          Navigation.pop(this.props.componentId);
        }
      });
    } else {
      this.setState({
        nameError: error.name[0]
      });
    }
  }

  datePicker = () => {
    this.setState({
      show: true
    });
  }

  setDate = (event, date) => {
    date = date || this.state.date;

    this.setState({
      show: false,
      date
    });
  }

  render() {
    const { name, nameError, date, show } = this.state;
    return (
      <ScrollView style={ApplicationStyles.container} keyboardShouldPersistTaps='always'>
        <TextInput
              style={ApplicationStyles.inputControl}
              placeholder={I18n.t('business')}
              value={name}
              keyboardType="default"
              maxLength={30}
              autoFocus
              selectionColor={Colors.black}
              placeholderTextColor={Colors.charcoal}
              underlineColorAndroid={Colors.transparent}
              onChangeText={(text) => this.setState({ name: text, nameError: '' })}
        />
          {nameError ?
            <Text style={ApplicationStyles.inputError}>{nameError}</Text> :
            null}
          <View
            style={[styles.view,
            { marginLeft: Metrics.doubleBaseMargin,
              marginTop: Metrics.baseMargin,
              marginBottom: Metrics.baseMargin }]}
          >
            <Text style={styles.textView}>{I18n.t('yearStart')}</Text>
            <View
              style={[ApplicationStyles.miniForm,
              { marginLeft: 0,
                marginTop: 0,
                marginBottom: 10,
                paddingTop: 0
              }]}
            >
              <TouchableOpacity
                style={{flex: 0.9}}
                onPress={this.datePickerDebounced}>
                  <TextInput
                      style={ApplicationStyles.inputControl}
                      placeholder={I18n.t('yearStart')}
                      multiline={false}
                      autoCorrect={false}
                      maxLength={25}
                      editable={false}
                      value={moment(date).format('DD MMM YYYY')}
                      selectionColor={Colors.black}
                      placeholderTextColor={Colors.charcoal}
                      underlineColorAndroid={Colors.transparent}
                      onChangeText={(text) => this.setState({ date: text })}
                  />
              </TouchableOpacity>
              <Icon
                name='calendar'
                size={Metrics.icons.medium}
                color={Colors.primary}
                style={ApplicationStyles.contactIcon}
                onPress={this.datePickerDebounced}
              />
            </View>
          </View>
          { show && <DateTimePicker
                      value={date}
                      maximumDate={new Date()}
                      onChange={this.setDate} />
          }
          <View style={ApplicationStyles.buttonWrapper}>
              <Button
                title={I18n.t('ok')}
                color={Colors.primary}
                onPress={this.onPressButtonDebounced}
              />
          </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  imageView: {
    margin: Metrics.doubleSection,
    alignItems: 'center',
    alignContent: 'center'
  },
  image: {
    width: Metrics.images.placeholder,
    height: Metrics.images.placeholder,
    resizeMode: 'contain'
  },
  view: {
    margin: Metrics.baseMargin,
    marginTop: 0,
    marginBottom: 0
  },
  textView: {
    color: Colors.charcoal,
    fontSize: 14,
    fontStyle: 'italic'
  }
});
