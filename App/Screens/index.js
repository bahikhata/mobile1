import { Navigation } from "react-native-navigation";

import Cash from "./Cash";
import Account from "./Account";
import Bank from "./Bank";

import New from "./New";

import AccountDetail from "./AccountDetail";

import EditName from "./EditName";
import AddAccount from "./AddAccount";
import EditAccount from "./EditAccount";
import AttachDetail from "./AttachDetail";
import EditTxn from "./EditTxn";

import Login from "./Login";

import Drawer from "./Drawer";
import Setting from "./Setting";
import OpenSource from "./Setting/OpenSource";
import Contact from "./Contact";

import Dummy from "./Dummy";
import Playground from "./Playground";

import Notification from "./Notification";
import ContactPicker from "../Components/ContactPicker";
import Topbar from "../Components/Topbar";

import CashTransfer from "./CashTransfer";

export function registerScreens() {
  Navigation.registerComponent("screen.pickContact", () => ContactPicker);
  Navigation.registerComponent("dummy", () => Dummy);
  Navigation.registerComponent("screen.cash", () => Cash);
  Navigation.registerComponent("screen.account", () => Account);

  Navigation.registerComponent("screen.new", () => New);
  Navigation.registerComponent("screen.newByAcc", () => NewByAcc);

  Navigation.registerComponent("screen.accDetail", () => AccountDetail);
  Navigation.registerComponent("screen.editName", () => EditName);
  Navigation.registerComponent("screen.editAccount", () => EditAccount);
  Navigation.registerComponent("screen.editTxn", () => EditTxn);
  Navigation.registerComponent("screen.addAccount", () => AddAccount);
  Navigation.registerComponent("screen.Drawer", () => Drawer);
  Navigation.registerComponent("screen.playground", () => Playground);
  Navigation.registerComponent("screen.attachDetail", () => AttachDetail);
  Navigation.registerComponent("screen.login", () => Login);
  Navigation.registerComponent("screen.bank", () => Bank);
  Navigation.registerComponent("lightbox.contact", () => Contact);
  Navigation.registerComponent("screen.notification", () => Notification);
  Navigation.registerComponent("topbar", () => Topbar);

  Navigation.registerComponent("screen.setting", () => Setting);
  Navigation.registerComponent("screen.setting.opensource", () => OpenSource);

  Navigation.registerComponent("screen.cashTransfer", () => CashTransfer);
}
