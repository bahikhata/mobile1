import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, Button, TextInput, ScrollView, ToastAndroid, Keyboard } from 'react-native';
import { Colors, Metrics, Images, ApplicationStyles } from '../Themes';
import { api } from '../Api';

const _ = require('lodash');
const I18n = require('../I18n');
import { Navigation } from 'react-native-navigation';

export default class EditTxn extends Component {
  static propTypes = {
    id: PropTypes.string.isRequired
  }
  constructor(props) {
    super(props);
    this.state = {
      detail: ''
    };
    this.onPressButtonDebounced = _.debounce(this.onPressButton, 300);
    this.navigationEventListener = Navigation.events().bindComponent(this);
  }

  componentDidMount() {
    const { id } = this.props;

    api.getTxnById(id).then(txn => {
      this.setState({
        detail: txn.detail
      });
    });
  }

  componentWillUnmount() {
    this.onPressButtonDebounced.cancel();
  }

  navigationButtonPressed({ buttonId }) {
    if (buttonId == "back"){
      this.goBack();
    }
  }

  onPressButton = () => {
    const { detail } = this.state;
    api.updateTransaction(this.props.id, detail, new Date());
    this.goBack();
  }

  goBack = () => {
    Keyboard.dismiss();
    Navigation.pop(this.props.componentId);
  }

  render() {
    const { detail } = this.state;
    return (
      <ScrollView
        style={ApplicationStyles.container}
        keyboardShouldPersistTaps='always'
      >
          <TextInput
            style={ApplicationStyles.inputControl}
              placeholder={I18n.t('detail')}
              ref='txn'
              value={detail}
              maxLength={35}
              autoFocus
              selectionColor={Colors.black}
              placeholderTextColor={Colors.charcoal}
              underlineColorAndroid={Colors.transparent}
              onChangeText={(text) => this.setState({ detail: text })}
          />
          <View style={ApplicationStyles.buttonWrapper}>
              <Button
                title={I18n.t('ok')}
                color={Colors.primary}
                onPress={this.onPressButtonDebounced}
              />
          </View>
      </ScrollView>
    );
  }
}
