import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text, Alert, Linking, Share, TouchableHighlight } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import CustomButton from '../Components/CustomButton';
import CustomButtonV from '../Components/CustomButtonV';
import { Colors, Metrics, Images, Fonts, ApplicationStyles } from '../Themes';
import { api } from '../Api';
import moment from 'moment';
import { iconsMap, iconsLoaded } from '../app-icons';
import { Navigation } from 'react-native-navigation';

const I18n = require('../I18n');

class Drawer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      date: ''
    };

    iconsLoaded.then(() => {

    });

    Navigation.events().registerComponentDidAppearListener(({componentId}) => {
      if(componentId === 'Component6' || componentId == 'Component8' || componentId == 'Component10'){
        this.setState({
          activeComponentId: componentId
        });
      }
    });

    this.navigationEventListener = Navigation.events().bindComponent(this);
  }

  componentDidMount() {
    this.setUserDetails();
  }

  componentDidAppear(){
    this.setUserDetails();
    this.forceUpdate();
  }

  setUserDetails = () => {
    api.getUser().then(user => {
      if (user) {
        this.setState({
          name: user.name,
          date: user.period
        });
      }
    });
  }

  toggleLeftDrawer = () => {
    Navigation.mergeOptions(this.props.componentId, {
      sideMenu: {
        left: {
          visible: false
        }
      }
    });
  }

  navigate = (name, title, props) => {
    this.toggleLeftDrawer();
    Navigation.push(this.state.activeComponentId, {
      component: {
        name: name,
        options: {
          topBar:{
            title: {
              text: title
            },
            leftButtons: {
              id: "back",
              icon: iconsMap["arrow-left"],
              iconColor: Colors.snow
            }
          },
          bottomTabs: {
            visible: false,
            drawBehind: true
          }
        },
        passProps: props
      }
    });
  }

  editName = () => {
    this.navigate("screen.editName", I18n.t("editBusiness"), null);
  }

  openShare = () => {
    Share.share({
      message: I18n.t('shareMessage'),
      title: I18n.t('shareTitle')
    });
  }

  openHelp = () => {
    this.toggleLeftDrawer();

    let url = "whatsapp://send?text=Hello&phone=+91-7456883325";
    Linking.canOpenURL(url)
      .then((supported) => {
        if(!supported){
          console.log('not supported');
        } else {
          Linking.openURL(url);
        }
      });
  }

  openSetting = () => {
    this.navigate("screen.setting", I18n.t("setting"), {});
  }

  cashTransfer = () => {
    this.navigate("screen.cashTransfer", I18n.t("cashTransfer"),{});
  }

  /*
  <TouchableHighlight
    style={styles.submenuButtonView}
    onPress={this.cashTransfer}
    underlayColor={Colors.underlayColor}>
    <Text style={styles.submenuButtonText}>Cash Transfer</Text>
  </TouchableHighlight>
  */

  render() {
    const { date, name } = this.state;
    return (
      <View style={[ApplicationStyles.container, styles.container]}>
        <View style={styles.header}>
          <View style={styles.subheader}>
            <Text style={styles.name}>{name}</Text>
            <Text style={styles.period}>
              { moment(date).format('DD MMM YYYY') + '  '} -
              { '  ' + moment(date).add(1, 'year').format('DD MMM YYYY')}
            </Text>
          </View>
          <Icon
              name='pencil'
              size={Metrics.icons.small}
              color={Colors.peach}
              style={styles.editButton}
              onPress={this.editName}
          />
        </View>
        <View style={styles.submenu} >

        </View>
        <View style={styles.menu}>
          <CustomButtonV text={I18n.t('support')} iconName="life-ring" onPress={this.openHelp} />
          <CustomButtonV text={I18n.t('share')} iconName="share-alt" onPress={this.openShare} />
          <CustomButtonV text={I18n.t('setting')} iconName="cog" onPress={this.openSetting} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: Metrics.drawerWidth
  },
  header: {
    flex: 0.2,
    width: Metrics.drawerWidth,
    backgroundColor: Colors.primary,
    flexDirection: 'row'
  },
  subheader: {
    flex: 0.9,
    flexDirection: 'column',
    marginLeft: Metrics.doubleBaseMargin,
    marginBottom: Metrics.baseMargin
  },
  editButton: {
    flex: 0.1,
    margin: Metrics.doubleBaseMargin,
    textAlignVertical: 'bottom'
  },
  name: {
    ...Fonts.style.h5,
    flex: 0.9,
    color: Colors.peach,
    textAlignVertical: 'bottom',
    marginBottom: Metrics.smallMargin
  },
  period: {
    flex: 0.2,
    fontSize: 12,
    fontStyle: 'italic',
    color: Colors.peach
  },
  submenu: {
    flex: 0.65,
    width: Metrics.drawerWidth
  },
  menu: {
    flex: 0.15,
    width: Metrics.drawerWidth,
    flexDirection: 'row',
    backgroundColor: Colors.primary,
  },
  menuItem: {
    flex: 0.333,
    flexDirection: 'column',
    alignItems: 'center',
    paddingBottom: Metrics.marginHorizontal,
    paddingTop: Metrics.doubleBaseMargin
  },
  menuButton: {
    paddingBottom: Metrics.marginHorizontal
  },
  menuText: {
    color: Colors.snow,
    fontSize: Fonts.size.regular
  },
  optionItem: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 15,
    paddingBottom: 15,
    paddingLeft: Metrics.doubleSection,
    backgroundColor: Colors.whitish,
    borderWidth: 0.2,
    borderColor: Colors.hairline
  },
  optionText: {
    color: Colors.black,
    fontSize: Fonts.size.regular,
    marginLeft: Metrics.doubleBaseMargin
  },
  submenuButtonView: {
    flex: 0.1,
    margin: Metrics.baseMargin,
    alignItems: 'center',
    borderWidth: 1
  },
  submenuButtonText: {
    fontSize: 16,
    fontWeight: 'bold',
    textAlignVertical: 'bottom'
  }
});

export default Drawer;
