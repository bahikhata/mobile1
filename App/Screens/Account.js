import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Text,
  View,
  Image,
  TouchableHighlight,
  Alert,
  FlatList,
  ToastAndroid,
  StyleSheet,
  ImageBackground
} from 'react-native';
import { iconsMap, iconsLoaded } from '../app-icons';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Colors, Metrics, Images, ApplicationStyles } from '../Themes';
import { Navigation } from 'react-native-navigation';

import { FloatingAction } from "react-native-floating-action";
const _ = require('lodash');
const accounting = require('accounting');
const I18n = require('../I18n');
import { api } from '../Api';

export default class Account extends Component {
  constructor(props) {
    super(props);

    this.state = {
        selected: -1
    };

    iconsLoaded.then(() => {
        this.initialMenu();
    });

    this.screenState = null;
    this.addAccountDebounced = _.debounce(this.addAccount, 300);
    this.viewAccountDebounced = _.debounce(this.viewAccount, 300);
    this.filterDebounced = _.debounce(this.filter, 300);

    this.navigationEventListener = Navigation.events().bindComponent(this);
  }

  componentDidMount(){
    this.fetchAccounts();

    api.getUser().then(user => {
      if(user){
        this.setState({
          userId: user.id
        });
      }
    });
  }

  componentWillUnmount() {
    this.addAccountDebounced.cancel();
    this.viewAccountDebounced.cancel();
    this.filterDebounced.cancel();

    if (this.navigationEventListener) {
      this.navigationEventListener.remove();
    }
  }

  componentDidAppear(){
    this.fetchAccounts();
  }

  navigate = (name, title, props) => {
    Navigation.push(this.props.componentId, {
        component: {
          name: name,
          options: {
            topBar: {
              title: {
                text: title
              },
              leftButtons: {
                id: "back",
                icon: iconsMap["arrow-left"],
                iconColor: Colors.snow
              }
            },
            bottomTabs: {
              visible: false,
              drawBehind: true
            }
          },
          passProps: props
        }
      });
  }

  navigationButtonPressed({ buttonId }) {
    if(buttonId == 'sideMenu'){
      this.toggleDrawer();
    } else if(buttonId == 'addAccount'){
      this.addAccountDebounced();
    } else if(buttonId == 'filter'){
      this.filterDebounced();
    } else if(buttonId == 'delete'){
      Alert.alert(
        '',
        I18n.t('closeAccMsg'),
        [
          { text: I18n.t('cancel'), onPress: () => this.dissmissContextualMenu() },
          { text: I18n.t('ok'), onPress: () => this.close(this.state.selected) }
        ],
        { cancelable: true }
      );
    } else if(buttonId == 'rename'){
      this.rename(this.state.selected);
    } else if(buttonId == 'close'){
      this.dissmissContextualMenu();
    }
  }

  toggleDrawer = () => {
    Navigation.mergeOptions(this.props.componentId, {
      sideMenu: {
        left: {
          visible: true
        }
      }
    });
  };

  dissmissContextualMenu = () => {
    this.setState({
      selected: -1
    });
    this.initialMenu();
  }

  initialMenu = () => {
    Navigation.mergeOptions(this.props.componentId, {
      topBar: {
        title: {
          text: I18n.t('accounts')
        },
        background: {
          color: Colors.primary
        },
        leftButtons: {
          id: 'sideMenu',
          icon: iconsMap.bars
        },
        rightButtons: [/*{
            id: 'filter',
            icon: iconsMap.filter
          }*/
        ]
      },
      bottomTab: {
        icon: iconsMap.users,
        iconColor: Colors.primary,
        selectedIconColor: Colors.lightPrimary
      }
    });
  }

  addAccount = () => {
    this.navigate("screen.addAccount", I18n.t("addAccount"), {userId: this.state.userId});
  }

  viewAccount = (id) => {
    if(this.state.selected > 0){
        this.dissmissContextualMenu();
    } else {
        this.navigate("screen.accDetail", "", {id: id});
    }
  }

  filter = () => {
      console.log('filter');
  }

  longPress = (rowId) => {
    this.setState({
      selected: rowId
    });
    Navigation.mergeOptions(this.props.componentId, {
      topBar: {
        title: {
          text: ""
        },
        background: {
          color: Colors.darkPrimary
        },
        leftButtons: {
          id: 'close',
          icon: iconsMap.times
        },
        rightButtons: [{
          id: 'delete',
          text: I18n.t('delete'),
          icon: iconsMap.trash
        },
        {
          id: 'rename',
          text: I18n.t('rename'),
          icon: iconsMap.pencil
        }]
      }
    });
  }

  rename = (id) => {
    this.dissmissContextualMenu();
    this.navigate("screen.editAccount", I18n.t("editAcc"), { id: id });
  }

  close = (id) => {
    api.getAccountById(id).then(account => {
      if (account.amount == 0) {
        api.closeAccount(account.id).then(result => {
          this.fetchAccounts();
        });
      } else {
        ToastAndroid.show(I18n.t('activeAccError'), ToastAndroid.SHORT);
      }
        this.dissmissContextualMenu();
    });
  }

  fetchAccounts = () => {
    api.getAccounts().then(accounts => {
      this.setState({
        accounts: accounts
      });
    });
  }

  renderSeparator = (index) => (
      <View key={index} style={ApplicationStyles.separator} />
  )

  renderItem = (rowData) => {
    let account = null;
    let amount = null;
    if(rowData.item.cancelled){
      account = <Text style={ApplicationStyles.accountCancel}>{rowData.item.name}</Text>
    } else {
      account = <Text style={[ApplicationStyles.account,{fontSize: 16}]}>{rowData.item.name}</Text>
    }
    if(rowData.item.cancelled){
      amount = <Text
        style={ApplicationStyles.amountCancel}
      >
        {accounting.formatMoney(Math.abs(rowData.item.amount), '₹', 0)}
      </Text>
    } else {
      amount = <Text
        style={[ApplicationStyles.amount,
          rowData.item.amount >= 0 ?
          { color: Colors.darkGreen, fontSize: 14, fontWeight: 'bold' }
          : { color: Colors.fire, fontSize: 14, fontWeight: 'bold'}
        ]}
      >
        {accounting.formatMoney(Math.abs(rowData.item.amount), '₹', 0)}
      </Text>
    }
    return(
        <TouchableHighlight
          underlayColor={Colors.underlayColor}
          key={rowData.item.id}
          onPress={() => this.viewAccountDebounced(rowData.item.id)}
          onLongPress={() => !rowData.item.cancelled ? this.longPress(rowData.item.id) : null}
        >
          <View
            style={[ApplicationStyles.rowAccount,
              this.state.selected === rowData.item.id ?
              { backgroundColor: Colors.lightPrimary, paddingTop: Metrics.middleBaseMargin, paddingBottom: Metrics.middleBaseMargin } :
              { backgroundColor: 'transparent', paddingTop: Metrics.middleBaseMargin, paddingBottom: Metrics.middleBaseMargin }]}
          >
            {account}
            {amount}
          </View>
        </TouchableHighlight>
    );
  }

  render() {
    const { accounts } = this.state;
    if(!accounts){
      return null;
    }
    return (
      <View style={[ApplicationStyles.container, { }]}>
          <ImageBackground
            style={styles.imageBack}
            source={Images.background}
            resizeMode='cover'>
            <FlatList
                style={{ flex: 1 }}
                data={accounts}//.sorted('name', false)}
                renderItem={this.renderItem}
                keyExtractor={item => item.id.toString()}
                ItemSeparatorComponent={this.renderSeparator}
                ListFooterComponent={() => (
                  accounts.length > 0 ?
                    <View style={ApplicationStyles.separator} />
                    : null
                )}
            />
          </ImageBackground>
          <FloatingAction
            color={Colors.primary}
            onPressMain={this.addAccount}
            showBackground={false}
            position="right"
            distanceToEdge={20}
            floatingIcon={<Icon name="user-plus" color={Colors.peach} size={22} />} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  imageBack: {
    flex: 1,
    width: '100%'
  }
});
