import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  TouchableHighlight,
  Text,
  View,
  StyleSheet,
  PermissionsAndroid,
  Platform,
  SafeAreaView,
  ScrollView,
  Keyboard
} from "react-native";
import { iconsMap, iconsLoaded } from '../app-icons';
import { Fonts, Colors, Metrics } from "../Themes";
import Contacts from "react-native-contacts";
import SearchBar from "./SearchBar";

import { Navigation } from 'react-native-navigation';

export default class ContactPicker extends Component {
  constructor(props){
    super(props);
    this.state = {
    }

    this.navigationEventListener = Navigation.events().bindComponent(this);
  }

  componentDidMount(){
    iconsLoaded.then(() => {
      Navigation.mergeOptions(this.props.componentId, {
        topBar: {
          leftButtons: {
            id: 'back',
            icon: iconsMap["arrow-left"]
          }
        }
      });
    });
    if (Platform.OS === "android") {
      PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_CONTACTS, {
        title: "Contacts",
        message: "This app would like to view your contacts."
      }).then(() => {
        this.loadContacts();
      });
    } else {
      this.loadContacts();
    }

  }

  navigationButtonPressed({ buttonId }) {
    if(buttonId == 'back'){
      Keyboard.dismiss();
      Navigation.pop(this.props.componentId);
    }
  }

  loadContacts = () => {
    Contacts.getAll((err, contacts) => {
      if (err === "denied") {
        console.warn("Permission to access contacts was denied");
      } else {
        var uniqueContacts = this.removeDuplicates(contacts);
        this.setState({ uniqueContacts });
      }
    });

    Contacts.getCount(count => {
      this.setState({ searchPlaceholder: `Search ${count} contacts` });
    });
  }

  removeDuplicates = (array) => {
    var uniqueArray = [];

    // Loop through array values
    for(let i=0; i < array.length; i++){
        if(uniqueArray.indexOf(array[i]) === -1) {
            uniqueArray.push(array[i]);
        }
    }
    console.log(uniqueArray);
    return uniqueArray;
  }

  search(text) {
    const phoneNumberRegex = /\b[\+]?[(]?[0-9]{2,6}[)]?[-\s\.]?[-\s\/\.0-9]{3,15}\b/m;
    if (text === "" || text === null) {
      this.loadContacts();
    } else if (phoneNumberRegex.test(text)) {
      Contacts.getContactsByPhoneNumber(text, (err, contacts) => {
        this.setState({ contacts });
      });
    } else {
      Contacts.getContactsMatchingString(text, (err, contacts) => {
        this.setState({ contacts });
      });
    }
  }

  render(){
    const { contacts } = this.props;
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView style={{ flex: 1 }}>
          {contacts.map(contact => {
            return (
              <TouchableHighlight
                style={{padding: 10, borderBottomWidth: 1}}
                key={contact.recordID}
                onPress={() => Contacts.openExistingContact(contact, () => {})}
              >
                <View style={styles.row}>
                  <Text style={styles.name}>
                    {contact.displayName}
                  </Text>
                  <Text style={styles.phone}>
                    {contact.phoneNumbers[0] != null ? contact.phoneNumbers[0].number : null}
                  </Text>
                </View>
              </TouchableHighlight>
            );
          })}
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  row: {
    flex: 1,
    flexDirection: 'row'
  },
  name: {
    flex: 0.6,
    fontSize: 16,
    fontWeight: 'bold'
  },
  phone: {
    flex: 0.4,
    fontSize: 16,
    fontStyle: 'italic',
    textAlign: 'right'
  }
});
