import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TouchableHighlight, Text, View, StyleSheet } from 'react-native';
import { Colors, Metrics, Fonts } from '../Themes';
import Icon from 'react-native-vector-icons/FontAwesome';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import { Navigation } from 'react-native-navigation';

export default class Topbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: new Date()
    };

    this.navigationEventListener = Navigation.events().bindComponent(this);
  }

  showPicker = () => {
    console.log("picker");
    this.setState({
      show: true
    });
  };

  setDate = (event, date) => {
    date = date || this.state.date;

    this.setState({
      show: false,
      date
    });

    if (date != undefined) {
      this.updateTxns();
    }
  }

  /*
  <TouchableHighlight
    style={styles.dateCalGroup}
    onPress={this.showPicker}>
      <>
        <Icon
            name='calendar'
            size={Metrics.icons.small}
            color={Colors.snow}
            style={styles.calendarMenu}
            onPress={() => this.showPicker()}
        />
        <Text
          style={styles.date}
          onPress={() => this.showPicker()}
        >
          {moment(date).format('DD MMM')}
        </Text>
        { show && <DateTimePicker
            value={date}
            minimumDate={period}
            maximumDate={new Date()}
            onChange={this.setDate} />
        }
      </>
    </TouchableHighlight>
    */

  render() {
    const { show, date } = this.state;
    return (
      <TouchableHighlight
        style={styles.dateCalGroup1}
        onPress={this.showPicker}>
        <>
          <Icon
              name='calendar'
              size={Metrics.icons.small}
              color={Colors.snow}
              style={styles.calendarMenu1}
              onPress={() => this.showPicker()}
          />
          <Text
            style={styles.date1}
            onPress={() => this.showPicker()}
          >
            {moment(date).format('DD MMM')}
          </Text>
          { show && <DateTimePicker
              value={date}
              minimumDate={new Date()}
              maximumDate={new Date()}
              onChange={this.setDate} />
          }
          </>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  calendarMenu1: {
    flex: 0.1,
    textAlignVertical: 'center',
  },
  date1: {
    flex: 0.3,
    fontSize: Fonts.size.h4,
    color: Colors.snow,
    fontStyle: 'italic',
    textAlignVertical: 'center'
  },
  dateCalGroup1: {
    flex: 1,
    flexDirection: 'row',
    borderWidth: 1,
    alignItems: 'center'
  }
});
