import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TouchableHighlight, Text, View, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Fonts, Colors, Metrics } from '../Themes';

export default class CustomButton extends Component {
  static propTypes = {
    text: PropTypes.string,
    iconName: PropTypes.string,
    onPress: PropTypes.func
  }

  render() {
    return (
      <TouchableHighlight onPress={this.props.onPress} underlayColor={Colors.underlayColor}>
        <View style={styles.optionItem}>
          <Icon
              name={this.props.iconName}
              size={Metrics.icons.medium}
              color={Colors.black}
          />
          <Text style={styles.optionText}>{this.props.text && this.props.text.toUpperCase()}</Text>
        </View>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  optionItem: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 15,
    paddingBottom: 15,
    paddingLeft: Metrics.doubleSection,
    backgroundColor: Colors.whitish,
    borderWidth: 0.2,
    borderColor: Colors.hairline
  },
  optionText: {
    color: Colors.black,
    fontSize: Fonts.size.regular,
    marginLeft: Metrics.doubleBaseMargin
  }
});
