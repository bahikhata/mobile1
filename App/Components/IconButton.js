import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TouchableHighlight, Text, View, StyleSheet, Button } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Fonts, Colors, Metrics } from '../Themes';

export default class IconButton extends Component {
  static propTypes = {
    text: PropTypes.string,
    iconName: PropTypes.string,
    onPress: PropTypes.func
  }

  render() {
    return (
      <TouchableHighlight onPress={!this.props.disabled ? this.props.onPress : null } underlayColor={Colors.primary} >
        <View style={!this.props.disabled ? styles.optionItem : styles.optionItemDisabled}>
          <Icon
              name={this.props.iconName}
              size={Metrics.icons.small}
              color={Colors.snow}
          />
          <Text style={styles.optionText}>{this.props.text && this.props.text.toUpperCase()}</Text>
        </View>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  optionItem: {
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: Colors.primary,
    borderWidth: 0.2,
    borderColor: Colors.hairline,
    borderRadius: 3,
    paddingLeft: Metrics.doubleBaseMargin * 2,
    shadowColor: Colors.windowTint,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
  },
  optionItemDisabled: {
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: Colors.windowTint,
    borderWidth: 0.2,
    borderColor: Colors.hairline,
    borderRadius: 3,
    paddingLeft: Metrics.doubleBaseMargin * 2
  },
  optionText: {
    color: Colors.snow,
    fontSize: Fonts.size.medium,
    marginLeft: Metrics.baseMargin,
    alignItems: 'center',
    textAlignVertical: 'center'
  }
});
