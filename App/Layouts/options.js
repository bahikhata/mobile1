
import { Colors } from "../Themes";

export const options = {
  statusBar: {
    visible: true,
    backgroundColor: Colors.primary
  },
  topBar: {
    visible: true,
    drawBehind: true,
    rightButtons: {
      color: Colors.peach
    },
    leftButtons: {
      color: Colors.peach
    },
    background: {
      color: Colors.primary
    },
    title: {
      fontSize: 20,
      color: Colors.peach
    }
  },
  animations: {
    push: {
      content: {
        x: {
          from: 2000,
          to: 0,
          duration: 400
        }
      }
    },
    pop: {
      content: {
        x: {
          from: 0,
          to: 2000,
          duration: 500
        }
      }
    }
  },
  bottomTabs: {
    visible: true,
    animate: true,
    drawBehind: false,
    backgroundColor: Colors.peach
  },
  bottomTab: {
    iconColor: Colors.primary,
    selectedIconColor: Colors.lightPrimary,
    color: Colors.primary,
    textColor: Colors.primary
  }
};
