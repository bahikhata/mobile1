const I18n = require('../I18n');

import { Colors, Fonts } from '../Themes';

export const register = {
  stack: {
    children: [
      {
        component: {
          name: "screen.login",
          options: {
            topBar: {
              visible: true,
              background: {
                color: Colors.primary
              },
              title: {
                text: I18n.t('app_name'),
                fontSize: 20,
                color: Colors.snow
              }
            }
          }
        }
      }
    ]
  }
};
