const I18n = require('../I18n');

import { Colors } from "../Themes";

export const playground = {
  stack: {
    children: [
      {
        component: {
          name: "screen.playground",
          options: {
            topBar: {
              visible: false
            }
          }
        }
      }
    ]
  }
}
