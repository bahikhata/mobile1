const I18n = require('../I18n');

import { Colors, Fonts } from '../Themes';

export const home = {
  sideMenu: {
    left: {
      component: {
        name: 'screen.Drawer'
      }
    },
    center: {
      bottomTabs: {
        id: 'tabs',
        children: [
          {
            stack: {
              children: [{
                component: {
                  name: "screen.cash",
                  options: {
                    topBar: {
                      visible: true,
                      title: {
                        text: I18n.t('cashbook')
                      }
                    },
                    bottomTab: {
                      text: I18n.t('cash'),
                      icon: require('../Images/swastika.png'),
                      iconColor: Colors.transparent,
                      selectedIconColor: Colors.transparent
                    }
                  }
                }
              }]
            }
          },
          {
            stack: {
              children: [
                {
                  component: {
                    name: "screen.account",
                    options: {
                      bottomTab: {
                        text: I18n.t("accounts"),
                        icon: require('../Images/swastika.png'),
                        iconColor: Colors.transparent,
                        selectedIconColor: Colors.transparent
                      },
                      topBar: {
                        title: {
                          text: I18n.t('accounts')
                        }
                      }
                    }
                  }
                }
              ]
            }
          },
          {
            stack: {
              children: [
                {
                  component: {
                    name: "screen.bank",
                    options: {
                      bottomTab: {
                        text: I18n.t('bank'),
                        icon: require('../Images/swastika.png'),
                        iconColor: Colors.transparent,
                        selectedIconColor: Colors.transparent
                      },
                      topBar: {
                        title: {
                          text: I18n.t('bank')
                        }
                      }
                    }
                  }
                }
              ]
            }
          }
        ]
      }
    }/*,
    right: {
      component: {
        name: 'screen.notification'
      }
    }*/
  }
};
