
import { home } from './home';
import { register } from './register'
import { options } from './options';
import { playground } from './playground';

export {
  home,
  register,
  options,
  playground
};
